Default Constants of UpperCode
==============================

## `BASIC_ROUTE`
This is absolute server route to basic folder of website
- Can be set in main index.php

**Important:** the last chart of returned string must be `'/'`
\
**Default value:** the folder of main `index.php` file

@see main `index.php` file

---

## `APP_ROUTE`
This is absolute server route to Applications folder

**Important:** the last chart of returned string is `'/'`
\
**Default value:** the folder of main `_applications` file

@see main `index.php` file

> Using in main framework router

---

## `DEBUG_MODE`
Flag of debugging mode
- Can be set in main index.php

**Default value:** `FALSE`

> Set it TRUE to view errors on screen

---

## `SAVE_LOG`
Flag of log saving mode
- Can be set in main index.php

**Default value:** `FALSE`

> Set it TRUE to save errors into log files

---

## `CORE_ROUTE`
This is absolute server route to core folder
- Is defined by framework
- Defined in `_core/core.php`

**Important:** the last chart of returned string is `'/'`
\
**Value:** root framework folder 

> It's used in settings, class autoloader and others core things 

---

## `UPPERCODE_VERSION`
Contain version of framework 
- Is defined by framework

---

## `HTTP_PROTOCOL`
Define http protocol
- Is defined by framework

**Allowed values:** `'http://'` or `'https://'`

> It's used while URLs defining

---

## `THEME_ROUTE`
This is absolute route to folder of web-site theme
- Is defined by framework

**Important:** the last chart of returned string is `'/'`
\
**Value:** `BASIC_ROUTE . $basic_config['THEME_FOLDER']`

> It's used for connection custom templates in uTPL

---

## `MEDIA_URL`
HTTP URL to web-site basic folder
- Can be set in main index.php

**Important:** the last chart of returned string must be `'/'`
\
**Default value:** `HTTP_PROTOCOL . $_SERVER['SERVER_NAME'] . '/' . $basic_config['BASIC_FOLDER']`

> It's used for connection theme files, like images, .css, .js etc.

---

## `HOMEPAGE`
HTTP URL to web-site home (main) page.
- Is defined by framework

**Important:** the last chart of returned string is `'/'`
\
**Value:** `HTTP_PROTOCOL . $_SERVER['SERVER_NAME'] . '/' . $basic_config['BASIC_FOLDER']`

> It's used in homepage defining

---

## `PAGE_URL`
HTTP URL of current page
- Is defined by framework

**Important:** full URI string, include all $_GET variables
\
**Value:** `HTTP_PROTOCOL . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']`

> It's used in homepage defining

---

## `CANONICAL_PAGE_URL`
Canonical URL of current page
- Is defined by framework

**Important:** the last chart of returned string is `'/'`
\
**Value:** `HTTP_PROTOCOL . $_SERVER['SERVER_NAME'] . $_SERVER['REDIRECT_URL']`

@see main `CORE_ROUTE . '_constants.php'` file

> It's used in URL building, PAGE_TYPE defining, pagination etc.

---

## `PAGE_TYPE`
Type of current page. 
- Is defined by framework
- if `HOMEPAGE == PAGE_URL`, `PAGE_TYPE` equal `'homepage'`

> It's used in URL building, module initialization, class autoloader, settings etc.
