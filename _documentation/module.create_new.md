Create new Module for UpperCode
===============================

## Check-list
1. Create module folder and Main class file of module
1. Create module Controller
1. Models connection
1. Views connection
1. Create View file


## 1. Create module folder and Main class file of module
First needs to choose Module name, for example `mymodule`. 

**NOTE** the module name has to be lowercase

Now you need to
- create main module controller - `APP_ROUTE . '_modules/mymodule/_controller.php'`,
- input there module controller class with class_name `MyModule`. It needs to use namespace `mod` and extends core abstract class `\uController`. Follow the required interface of `\uController`
```
<?php
/**
 * @link <module web-site>
 * @license GNU GPL v3 <link to full license text>
 * @copyright Copyright (c) 2017 Your Name (your web-site)
 * @author Your Name <your@email.address>
 **/

namespace mod;

class MyModule extends \uController
{
	protected function init_module()
	{
		//Will calls with init_module action in main framework controller  
	}
	
	protected function defined_modules() {
		//Will calls after all module has been inited by main framework controller
	}
	
	protected function init_page() {
		//Will calls with init_page action in main framework controller  
	}
}
```


## 2. Create module Controller
We recommend you to use main actions for module initialisation:
- `defined_modules` - first action after including all needed modules for current PAGE_TYPE

**If (PAGE_TYPE != 'api'):** (does not using for REST API)
- `init_page` - next running after `defined_modules`, this action calls after main controller action `\uController::runModuleActions(\uController::ACT_INIT_PAGE);`
- `page_init_completed` - the last main action in page initialisation process


**If (PAGE_TYPE == 'api'):** (Only for REST API)
- `init_api` - calls before API initialisation
- `api_init_completed` - is the last main action in API init process. Calls after API initialisation

> Use this hooks, it allow you to init your module after modules defining process, when all settings is available

> !!! **Do not use name `builder()`** for module controller class method - it will brock algorithm of main controller

```
class MyModule extends \uController
{
	protected function init_module()
	{
		uCode::add_action('page_init_completed', ['\mod\MyModule' => 'init']); // you can call through action only public methods
	}

	protected function defined_modules() {
			//Will calls after all module has been inited by main framework controller
		}
		
		protected function init_page() {
			//Will calls with init_page action in main framework controller  
		}

	static private function init() //this method will run in `init_page` action
	{
		//do some code
	}
}
```

## 3. Model connection
You need to put in model file to `_models/` folder in root of module folder and use to connect it controller default method `getModel()`

```
// Inside of file `APP_ROUTE . '_modules/mymodule/_controller.php'`

	static private function init() //this method will run in `init_page` action
	{
		$my_module_object = uCode::getModuleObject('mymodule'); // get module object, it needs only for static methods, in dynamic methods you should use `$this`

		$my_module_object->getModel('example_model.php'); //this method will connect file `APP_ROUTE . '_modules/mymodule/_models/example_model.php'` (model from current module folder)
	}

```


## 4. Views connection
There is the similar way, as for models, but there it no default controller methods. To connect view, needs dynamic method `$this->getView()`.
This method will connect the file from folder `_views/` in root of module folder

```
// Inside of file `APP_ROUTE . '_modules/mymodule/_models/example_model.php'`

PData::setPD('title', 'Error Page'); //set page title

$message_html = $this->getView('message_tpl', ['message_сode' => 1, 'messages' => 'The user has curved hands']); // will connect the file `APP_ROUTE . '_modules/mymodule/_views/message_tpl.php'`
PData::setPD('my_message', $message_html); //set rendered html into page data cache.

```


## 5. Create View file
In previous item we connect veiw file and transferred array `['сode' => 1, 'messages' => 'The user has curved hands']`, inside the view file we can get this data follow next way

```
// Inside of file `APP_ROUTE . '_modules/mymodule/_views/message_tpl.php'`

echo $message_сode; //it will print `1`
echo $messages; //it will print 'The user has curved hands'
print_r($data); // it will print `['сode' => 1, 'messages' => 'The user has curved hands']`


echo $data_log;
/* it will print full list of transferred element keys
`$message_сode;
$messages;`
*/
```

**Example:**
```
// Inside of the file `APP_ROUTE . '_modules/mymodule/_views/message_tpl.php'`

<p><b>Error code:</b> <?php echo $message_сode; ?></p>
<p><b>Message:</b> <?php echo $messages; ?></p>

```


## 6. Connect Additional module class
To use an Additional class, you just need to follow next steps.

**Simple steps**
- Create a module class in classes folder - `APP_ROUTE . '_modules/mymodule/_classes/my_class.php'`,
- Set namespace of this class: `mod\MyModule`,
- Create class with the same name, as class file name.


```
// Inside of the file `APP_ROUTE . '_modules/mymodule/_classes/my_class.php'`

namespace mod\MyModule;

class my_class
{
//class body
}
```

To calling this class outside, needs to call class name with namespace
```
$object = new \mod\MyModule\my_class();
```


To put class file into sub-folder of module root folder add the name of this sub-folder to namespace
```
// Inside of file `APP_ROUTE . '_modules/mymodule/_classes/sub_folder/my_class.php'`

namespace mod\MyModule\sub_folder;

class my_class
{
//class body
}
```


## 7. Connect template (theme) file
In previous steps we created module and prepare html.
Now we need to connect theme file to load front-end.
For this we need to use method `uTPL::setBasicTPL()`.

```
uTPL::setBasicTPL('error_page.php'); //will connect `THEME_ROUTE . 'error_page.php'`
```

This method has 2 modes - you can set absolute route to template file or set only name of tpl file from THEME_ROUTE folder


## 8. Create template (theme) file
This is the front-end file
```
<!-- Inside of file `THEME_ROUTE . 'error_page.php'` -->

<h1><?php echo PData::getPD('title'); ?></h1>
<?php echo PData::getPD('my_message'); ?>

```
