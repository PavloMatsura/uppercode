Settings of UpperCode
=====================


## homepage_settings
Containing settings of homepage

This settings is use, when PData::isHomapage() return TRUE

### Example: 
```
'homepage_settings' => array(
		'page_type' => 'homepage', // page type, which needs to load on homepage
		'alias' => 'home', // value of $_GET[PAGE_TYPE]
		'exception_get_variables' => array( //$_GET elements list, which need to be ignored when homepage page type defined (in PData::isHomapage())
			'utm_source',
			'utm_source',
			'utm_medium',
			'utm_campaign',
		),
	),
```
---

## allowed_page_types
Framework will ignore all page types, which doesn't exists in this list

### Example: 
```
'allowed_page_types' => array(
		'homepage',
		'api',
	),
```

---

## db_settings
Settings of Data Base

### Example: 
```
'db_settings' => array(
		'default' => [ // Default DB connection access settings
			'host' => 'localhost',
			'user' => 'root',
			'pass' => '',
			'db_name' => 'db_uppercode',
		],
		'custom_connection' => [ // If needs to connect to other DB
			'host' => 'localhost',
			'user' => 'some_username',
			'pass' => 'some_password',
			'db_name' => 'db_uppercode_1',
		],
	),
```

---

## classes_allowed_rules [autoloader]
This setting used if needs to allow access to some class only for specific page types  

### Example: 
```
'classes_allowed_rules' => array(
		'\mod\module_name\class_file_name' => array(
			'homepage',
			'api',
		),
	),
```

---

## libs_allowed_rules [autoloader]
The list of allowed library classes for each PAGE_TYPE.
If class isn't set in this array - it is allowed for all PAGE_TYPEs. 

**Important:** Lib class must be in `APP_ROUTE . '_libraries/`.
**How to use:** Input library class file in root of `APP_ROUTE . '_libraries/'` or in sub-folder of it, sub-folder must has the same name as library class file (`APP_ROUTE . '_libraries/my_lib/my_lib.php'`).
Library class must have `lib` namespace (`'lib\my_lib()'`)

### Example: 
```
'classes_allowed_rules' => array(
		'my_lib' => array(
			'homepage',
			'api',
		),
	),
```

---

## include_apps
This list determines, which application needs to be included

### Example: 
```
'include_apps' => array(
		BASIC_ROUTE . 'vendor/autoload.php' => array(), //allowed for all PAGE_TYPEs
		BASIC_ROUTE . 'my_scr.php' => array( //allowed only for 'homepage' and 'api'
			'homepage',
			'api',
		),
	),
```

---

## include_modules
This list contains Module names, which needs to be include for specific page types  
 
**Important:** module name must contains only `/a-zA-Z0-9_/` charts

### Example: 
```
'include_apps' => array(
	'include_modules' => array(
		'example_module' => array(), //allowed for all PAGE_TYPEs
		'example_module_2' => array( //allowed only for 'homepage' and 'api'
			'homepage',
			'api',
		),
	),
```

---

## init_callbacks
Early activity
Calling functions and methods after config defined
 
> This is the first actions, which is in page loading

### Example: 
```
'init_callbacks' => [
		//Function calling format
		'functionName' => [
			'param1' => 1,
			'param2' => 2,
		],

		//Method calling format [autoloader]
		[
			'class_name' => 'my_class', //includes namespace
			'method_name' => 'my_method',
			'param1' => 1,
			'param2' => 2,
			//...
		],
		
		'action' => [
			'init_page' => [
				//Function calling format
				'functionName',

				//Method calling format [autoloader]
				['\mod\admin', 'addMenuItem'],
				['\mod\admin', 'addMenuItem'],
			],
		],

		'filter' => [
			'init_page' => [
				//Function calling format
				'functionName',

				//Method calling format [autoloader]
				['\mod\admin', 'addMenuItem'],
			],
  		],
	],
```

---

## URL_rules
//ToDo

`{category_alias}` - must contains only `/a-zA-Z0-9_\-\./` charts
`[post_alias]` - must contains only `/a-zA-Z0-9_\-\.\//` charts. It can including only once and must be the last variable in URL rule string

### Example: 
```
'URL_rules' => array(
		'category' => 'cat/{category_alias}/[post_alias]/',
		'page' => '{page_alias}/',
		'post' => '{category_alias}/{post_alias}.html',
	),
```

---

