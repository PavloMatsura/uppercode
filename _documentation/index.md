UpperCode anatomy
=================

## Main classes
- [uCode] - main class. Is responsible for core initialization and routing
- [PData] - class of current page data
- [uTPL] - connect tlp
- [uController] - adstract class. Is responsible for modules initialization
- [uDB] - class-wrapper of PDO class
- [uDBMigration] - Is responsible for db migration
- [uAPI] - REST API builder
- [uBugtracker] - bugtracker system

## Addition classes
- [uCookie] - works with $_COOKIE


## Default sets
- [_fn.php] - default framework functions
- [Constants](_constants.md) - default framework constants
- [Settings](_settings.md) - framework settings


## Algorithms
- Autoloader of classes
- Debugging 
	+ Exceptions
- Hooks - to get full hooks list, print var `uCode::$hooks_init_log`
	+ Callbacks - see [Settings](_settings.md) -> Part `init_callbacks`
	+ Actions - see `uCode::add_action()` and `uCode::run_action()`
	+ Filters - see `uCode::add_filter()` and `uCode::run_filter()`
- Logs
- Modules
	+ [Create new Module](module.create_new.md)
	+ Settings
	+ REST API
- Page URLs
- Web-site theme