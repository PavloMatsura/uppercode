<?php
/**
 * Connect classes
 */
spl_autoload_register(function ($class_name)
{
	$routes_list = [];
	$class_route= '';
	$class_file_name = strtr($class_name, '\\', '//');

	switch (substr($class_file_name, 0, 3)) {
		case 'mod':
			$class_file_name = substr($class_file_name, 4);
			$stripos = stripos($class_file_name, '/');

			$class_file_name_lower = mb_strtolower($class_file_name);

			//Main module class
			$routes_list = [
				APP_ROUTE . '_modules/' . $class_file_name . '/_controller.php',
				APP_ROUTE . '_modules/' . $class_file_name_lower . '/_controller.php',
			];

			//Additional module classes
			if ($stripos) {
				$class_file_name = substr_replace($class_file_name, '/_classes', $stripos, 0);
				$class_file_name_lower = substr_replace($class_file_name_lower, '/_classes', $stripos, 0);

				$routes_list = [
					APP_ROUTE . '_modules/' . $class_file_name . '.php',
					APP_ROUTE . '_modules/' . $class_file_name_lower . '.php',
				];
			}
			break;

		default:
			$class_file_name_lower = mb_strtolower($class_file_name);

			$routes_list = [
				CORE_ROUTE . "classes/{$class_file_name}.php",
				CORE_ROUTE . "classes/{$class_file_name_lower}.php",
				APP_ROUTE . "_libraries/{$class_file_name}/{$class_file_name}.php",
				APP_ROUTE . "_libraries/{$class_file_name_lower}/{$class_file_name_lower}.php",
				APP_ROUTE . "_libraries/{$class_file_name}.php",
				APP_ROUTE . "_libraries/{$class_file_name_lower}.php",
			];
			break;
	}

	foreach ($routes_list as $route) {
		if (file_exists($route)) {
			$class_route = $route;

			break;
		}
	}

	if (!empty($class_route)) {
		require_once $class_route;
	}

	if (0 !== strpos($class_name, 'Twig')) {
		return;
	}

	$file_name = CORE_ROUTE . 'classes/'.str_replace(array('_', '\\'), array('/', ''), $class_name).'.php';

	if (file_exists($file_name)) {
		require_once $file_name;

		return;
	}

	$low_file_name = strtolower($file_name);

	if (file_exists($low_file_name)) {
		require_once $low_file_name;
	}
});

