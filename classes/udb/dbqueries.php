<?php

namespace uDB;

use \PDO, \uDB, \PData;

/**
 * Class DBQueries
 *
 * @package uDB
 *
 * @property $response
 */
class DBQueries
{
	const UPDATE_TYPE = 'update';
	const INSERT_TYPE = 'insert';
	const REPLACE_TYPE = 'replace';
	const DELETE_TYPE = 'delete';

	//Connections catche
	private static $connections_config = [];

	/**
	 * @var DBQueries[]
	 */
	private static $connections = FALSE;

	//Connection session
	private $connection_name = FALSE;
	private $query_result = FALSE;

	/**
	 * @var PDO
	 */
	private $current_connection = FALSE;

	//Temp properties
	private $query = FALSE;
	private $where = FALSE;

	/**
	 * @var bool|array
	 */
	private $binds = FALSE;
	private $no_duplication = FALSE;
	private $query_type = FALSE;
	private $response = FALSE;

	/**
	 * @param string $connection_name
	 *
	 * @return DBQueries
	 */
	static public function init($connection_name)
	{
		if (isset(self::$connections[$connection_name]) && is_object(self::$connections[$connection_name])) {
			return self::$connections[$connection_name];
		}

		$object = new self();
		$object->connection_name = $connection_name;

		### Get config ===
		self::$connections_config = array_merge(self::$connections_config, PData::config('db_settings'));

		if (empty(self::$connections_config[$connection_name]) || empty(self::$connections_config[$connection_name]['host']) || empty(self::$connections_config[$connection_name]['user']) || !isset(self::$connections_config[$connection_name]['pass']) || empty(self::$connections_config[$connection_name]['db_name'])) {

			throw new \InvalidArgumentException('Invalid "' . $connection_name . '" DB connection settings');
		}

		$host = self::$connections_config[$connection_name]['host'];
		$user = self::$connections_config[$connection_name]['user'];
		$pass = self::$connections_config[$connection_name]['pass'];
		$db_name = self::$connections_config[$connection_name]['db_name'];

		if (!isset(self::$connections_config[$connection_name]['charset'])) {
			self::$connections_config[$connection_name]['charset'] = 'UTF8';
		}

		$charset = self::$connections_config[$connection_name]['charset'];

		$dsn = "mysql:host={$host};dbname={$db_name};charset={$charset}";
		$opt = array(
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		);


		$object->current_connection = new PDO($dsn, $user, $pass, $opt);

		return self::$connections[$connection_name] = $object;
	}

	private function __construct() {}

	public function __get($property_name)
	{
		switch ($property_name) {
			case 'response':
				return $this->response;
			default:
				throw new \InvalidArgumentException("Property '{$property_name}' does not exist");
		}
	}

	/**
	 * Begin MySQL Transaction
	 *
	 * @return DBQueries
	 */
	public function begin()
	{
		$this->current_connection->beginTransaction();

		return $this;
	}

	/**
	 * Commit MySQL Transaction
	 *
	 * @return DBQueries
	 */
	public function commit()
	{
		$this->current_connection->commit();

		return $this;
	}

	/**
	 * Rollback MySQL Transaction
	 *
	 * @return DBQueries
	 */
	public function rollback()
	{
		$this->current_connection->rollBack();

		return $this;
	}

	/**
	 * Set DB query
	 *
	 * @param string $query
	 *
	 * @return DBQueries
	 *
	 * @throws InvalidArgumentException
	 *          if DB connection is not exists
	 **/
	public function query($query)
	{
		if (!is_string($query) || empty($query)) {
			throw new \InvalidArgumentException('$query must be not empty string');
		}

		$this->query = $query;

		return $this;
	}

	/**
	 * Make Update Query
	 *
	 * @param string $table - table name or JOINed tables, which has the same column names
	 *
	 * @return DBQueries
	 *
	 * @throws \InvalidArgumentException
	 *			if $table is empty or not string
	 **/
	public function update($table)
	{
		if (!is_string($table) || empty($table)) {
			throw new \InvalidArgumentException('$table must be not empty string');
		}

		$this->query_type = self::UPDATE_TYPE;
		$this->table = $table;

		return $this;
	}

	/**
	 * Make Insert Query
	 *
	 * @param string $table - table name or JOINed tables, which has the same column names
	 * @param mixed $no_duplication - set uDB::NO_DUPLICATION , if needs to update available rows (works through 'ON DUPLICATE KEY')
	 *
	 * @return DBQueries
	 *
	 * @throws \InvalidArgumentException
	 *			if $table is empty or not string
	 **/
	public function insert($table, $no_duplication = FALSE)
	{
		if (!is_string($table) || empty($table)) {
			throw new \InvalidArgumentException('$table must be not empty string');
		}

		$this->query_type = self::INSERT_TYPE;
		$this->table = $table;
		$this->no_duplication = $no_duplication;

		return $this;
	}

	/**
	 * Make Repleace Query
	 *
	 * @param string $table - table name or JOINed tables, which has the same column names
	 *
	 * @return DBQueries
	 *
	 * @throws \InvalidArgumentException
	 *			if $table is empty or not string
	 **/
	public function replace($table)
	{
		if (!is_string($table) || empty($table)) {
			throw new \InvalidArgumentException('$table must be not empty string');
		}

		$this->query_type = self::REPLACE_TYPE;
		$this->table = $table;

		return $this;
	}

	/**
	 * Make Delete Query
	 *
	 * @param string $table - table name or JOINed tables
	 *
	 * @return DBQueries
	 *
	 * @throws \InvalidArgumentException
	 *			if $table is empty or not string
	 **/
	public function delete($table)
	{
		if (!is_string($table) || empty($table)) {
			throw new \InvalidArgumentException('$table must be not empty string');
		}

		$this->query_type = self::DELETE_TYPE;
		$this->query = "	DELETE FROM {$table} ";

		return $this;
	}

	/**
	 * Set values, which needs to send to DB
	 *
	 * @param array $values
	 * @param string $column_name_prifix - set in parameter, if using prefix of table column names
	 *
	 * @return DBQueries
	 *
	 * @throws \InvalidArgumentException
	 * 			if used not for allowed methods: update(), insert(), replace()
	 *			if $values is empty or not array
	 * 			if $column_name_prifix is not string
	 *
	 **/
	public function values($values, $column_name_prifix = '')
	{
		if (!in_array($this->query_type, [self::UPDATE_TYPE, self::INSERT_TYPE, self::REPLACE_TYPE])) {
			throw new \InvalidArgumentException('Method values() allow to use only after methods: update(), insert(), replace()');
		}

		if (empty($values) || !is_array($values)) {
			throw new \InvalidArgumentException('$values must be not empty array');
		}

		if (!is_string($column_name_prifix)) {
			throw new \InvalidArgumentException('$column_name_prifix must be string');
		}

		switch ($this->query_type) {
			case self::UPDATE_TYPE:
				$this->set_update_values($values, $column_name_prifix);
			break;

			default:
				$this->set_insert_replace_values($values, $column_name_prifix);
			break;
		}

		return $this;
	}

	/**
	 * Pripare values for Update query
	 *
	 * @param array $values
	 * @param string $column_name_prifix - set in parameter, if using prefix of table column names
	 *
	 * @return void
	 *
	 * @throws \InvalidArgumentException
	 * 			if column name ($key) is empty or not string
	 *
	 **/
	private function set_update_values($values, $column_name_prifix = '')
	{
		$update_query = '';
		$rendom_str = _getRandStr(4);
		$i = 0;

		foreach ($values AS $key => $value) {
			if (!is_string($key) || $key == '') {
				throw new \InvalidArgumentException('Keys of $values array must be not empty strings for update()');
			}

			if ($i > 0) {
				$update_query .= ',';
			}

			$update_query .= " `" . str_replace('.', '`.`', $column_name_prifix . $key) . "` = :update_{$rendom_str}_val_{$i} ";
			$update_vars[":update_{$rendom_str}_val_{$i}"] = $value;

			$i++;
		}

		$this->query = "	UPDATE {$this->table} SET {$update_query} ";

		$this->binds($update_vars);
	}

	/**
	 * Prepare values for Insert or Replace query
	 *
	 * @param array $values
	 * @param string $column_name_prifix - set in parameter, if using prefix of table column names
	 *
	 * @return void
	 *
	 * @throws \InvalidArgumentException
	 * 			if column name ($key) is empty or not string
	 *
	 **/
	private function set_insert_replace_values($values, $column_name_prifix = '')
	{
		$update_query = $columns = $col_values = '';
		$insert_vars = array();
		$rendom_str = _getRandStr(4);
		$i = 0;

		foreach ($values AS $key => $value) {
			if (!is_string($key) || $key == '') {
				throw new \InvalidArgumentException('Keys of $values array must be not empty strings for ' . $this->query_type . '()');
			}

			if ($i > 0) {
				$columns .= ', ';
				$col_values .= ',';
				$update_query .= ', ';
			}

			$column_name = ' `' . str_replace('.', '`.`', $column_name_prifix . $key) . '` ';
			$columns .= $column_name;
			$col_values .= " :insert_{$rendom_str}_val_{$i} ";

			$update_query .= " {$column_name} = :insert_{$rendom_str}_val_{$i} ";
			$insert_vars[":insert_{$rendom_str}_val_{$i}"] = $value;

			$i++;
		}

		switch ($this->query_type) {
			case self::INSERT_TYPE:
				if ($this->no_duplication == uDB::NO_DUPLICATION) {
					$this->query = "	INSERT INTO `{$this->table}` ({$columns}) VALUES ({$col_values}) ON DUPLICATE KEY UPDATE {$update_query};";
				} else {
					$this->query = "	INSERT INTO `{$this->table}` ({$columns}) VALUES ({$col_values});";
				}
			break;

			case self::REPLACE_TYPE:
				$this->query = "	REPLACE INTO `{$this->table}` ({$columns}) VALUES ({$col_values});";
			break;
		}

		$this->binds($insert_vars);
	}

	/**
	 * Set "WHERE" conditions
	 *
	 * @param string $conditions
	 *
	 * @return DBQueries
	 *
	 * @throws \InvalidArgumentException
	 *			if used not for allowed methods: update(), delete()
	 * 			if "WHERE" conditions exists before, for current query
	 **/
	public function where($conditions = '')
	{
		if (!in_array($this->query_type, [self::UPDATE_TYPE, self::DELETE_TYPE])) {
			throw new \BadMethodCallException('Method where() allow to use only after methods: update(), insert(), repleace(), delete()');
		}

		if (!empty($conditions)) {
			if ($this->where) {
				throw new \InvalidArgumentException('Current query ' . $this->query_type . '() already using "WHERE" conditions');
			}

			$this->where = " WHERE {$conditions} ";
		}

		return $this;
	}

	/**
	 * Prepare binds array
	 *
	 * @param array|string $binds_array
	 *
	 * @return DBQueries
	 *
	 * @throws \InvalidArgumentException
	 * 			if $binds_array is empty or not array
	 * 			if bind placeholder is empty or not string
	 *			if bind value is not scalar
	 **/
	public function binds($binds_array, $param = '')
	{
		if (is_string($binds_array)) {
			$this->binds([$binds_array => $param]);
		} elseif (is_array($binds_array) && !empty($binds_array)) {
			foreach ($binds_array AS $placeholder => $bind_value) {

				if (!is_string($placeholder) || empty($placeholder)) {
					throw new \InvalidArgumentException('Invalide bind placeholder - ' . $placeholder . ' should be not empty string');
				}

				if ($placeholder[0] != ':') {
					$placeholder = ':' . $placeholder;
				}

				if (is_scalar($bind_value) || $bind_value == NULL) {
					$this->binds[$placeholder] = $bind_value;
				} elseif (is_array($bind_value)) {
					$binds_keys = [];

					foreach ($bind_value as $key => $val) {
						if (!is_scalar($val)) {
							throw new \InvalidArgumentException('Invalide bind value - ' . $placeholder . ' -> ' . $key . ' should be scalar');
						}

						$this->binds[$placeholder . $key] = $val;
						$binds_keys[] = $placeholder . $key;
					}

					if (!empty($binds_keys)) {
						$this->query = str_replace($placeholder, implode(',', $binds_keys), $this->query);
						$this->where = str_replace($placeholder, implode(',', $binds_keys), $this->where);
					}
				} else {
					throw new \InvalidArgumentException('Invalide bind value - ' . $placeholder . ' should be scalar or array');
				}
			}
		} else {
			throw new \InvalidArgumentException('Invalide parameter - $binds_array should be not empty array');
		}

		return $this;
	}

	/**
	 * Execute prepared query
	 *
	 * @return DBQueries
	 **/
	public function execute()
	{
		if ($this->where) {
			$this->query .= ' ' . $this->where;
		}

		$this->query_result = $this->current_connection->prepare($this->query);

		if (!empty($this->binds)) {
			foreach ($this->binds as $placeholder => $value) {
				$this->query_result->bindValue($placeholder, $value);
			}
		}

		$this->response = $this->query_result->execute();

		//Reset temp properties
		$this->where = FALSE;
		$this->query = FALSE;
		$this->binds = FALSE;
		$this->query_type = FALSE;
		$this->no_duplication = FALSE;

		return $this;
	}

	/**
	 * Dump prepared query
	 *
	 * @return uDB\DBDump
	 **/
	public function dump($table_name, $sql_statement)
	{
		if (!is_string($table_name)) {
			throw new \InvalidArgumentException('Invalid $table_name. Must be string');
		}

		switch ($sql_statement) {
			case uDB::TYPE_INSERT:
			case uDB::TYPE_UPDATE:
			case uDB::TYPE_REPLACE: break;

			default:
				throw new \InvalidArgumentException('Not allowed value SQL Statement');
		}

		## Defining WHERE condition ==

		$dump_where = (string) $this->where;

		if (empty($dump_where)) {
			$dump_where = explode('WHERE', $this->query);

			if (count($dump_where) == 1) {
				$dump_where = explode('where', $this->query);
			}

			if (count($dump_where) == 2) {
				$dump_where = $dump_where[1];
			}

			if (is_array($dump_where)) {
				$dump_where = $dump_where[0];
			}
		}

		$result = $this->execute()
			->fetch_array();

		return DBDump::init($table_name, $result, $sql_statement)
			->where($dump_where);
	}

	/**
	 * Return row count
	 **/
	public function rowCount()
	{
		return $this->query_result->rowCount();
	}

	/**
	 * Get row array
	 **/
	public function fetch($style = 'FETCH_ASSOC', $cursor_orientation = PDO::FETCH_ORI_NEXT, $cursor_offset = 0)
	{
		return $this->query_result->fetch(constant('PDO::' . $style), $cursor_orientation, $cursor_offset);
	}

	/**
	 * Get value of 1 column
	 *
	 * @param string|int $key
	 *
	 * @return mixed|null
	 *
	 * @throws \InvalidArgumentException
	 *          if column with name $key was not found in request result
	 **/
	public function fetch_value($key = 0)
	{
		$result = $this->fetch('FETCH_BOTH');

		if (!$result) {
			return NULL;
		}

		if (!is_string($key) && !is_int($key)) {
			throw new \InvalidArgumentException('Parameter $key should be int or not empty string');
		} elseif (!array_key_exists($key, $result)) {
			throw new \InvalidArgumentException('Column "' . $key . '" was not found in request result');
		}

		return $result[$key];
	}

	/**
	 * Get rows array
	 *
	 * @param mixed $key - name or number of column, which value needs to be key of arrays elements
	 *
	 * @return array
	 *
	 * @throws \InvalidArgumentException
	 *          if column with name $key was not found in request result
	 **/
	public function fetch_array($key = NULL)
	{
		$result = [];

		if (is_string($key)) {
			$tmp_result = $this->query_result->fetchAll();

			if (!empty($tmp_result)) {
				if (!array_key_exists($key, $tmp_result[0])) {
					throw new \InvalidArgumentException('Column "' . $key . '" was not found in request result');
				}

				foreach ($tmp_result as $cval) {
					$result[$cval[$key]] = $cval;
				}

				unset($cval);
			}

			unset($tmp_result);
		} elseif (is_int($key)) {
			$result = $this->query_result->fetchAll(NULL, $key);
		} else {
			$result = $this->query_result->fetchAll();
		}

		return $result;
	}

	/**
	 * Get rows array
	 *
	 * @link http://php.net/manual/en/pdostatement.fetchall.php
	 **/
	public function fetchAll($style = false, $fetch_argument = NULL, $ctor_args = [])
	{
		if (is_string($style)) {
			$style = constant('PDO::' . $style);
		} else {
			$style = NULL;
		}

		if ($fetch_argument && !empty($ctor_args)) {
			$result = $this->query_result->fetchAll($style, $fetch_argument, $ctor_args);
		} elseif ($fetch_argument) {
			$result = $this->query_result->fetchAll($style, $fetch_argument);
		} else {
			$result = $this->query_result->fetchAll($style);
		}

		return $result;
	}

	/**
	 * Get value of column $value
	 *
	 * @param string $value - column name, which value needs to be value of arrays element
	 * @param string|int|false $key - column name, which value needs to be key of arrays element
	 *
	 * @return array
	 *
	 * @throws \InvalidArgumentException
	 *          if parameter $value is not string or empty
	 *          if column with name $value was not found in request result
	 *          if column with name $key was not found in request result
	 **/
	public function fetch_column($value, $key = FALSE)
	{
		if (!is_string($value)) {
			throw new \InvalidArgumentException('Parameter $value should be not empty string');
		}

		$result = [];

		$tmp_result = $this->query_result->fetchAll();

		if (empty($tmp_result)) {
			return $result;
		}

		if (!array_key_exists($value, $tmp_result[0])) {
			throw new \InvalidArgumentException('Column "' . $value . '" was not found in request result');
		}

		if ($key && !array_key_exists($key, $tmp_result[0])) {
			throw new \InvalidArgumentException('Column "' . $key . '" was not found in request result');
		}

		if (!$key) {
			$result = array_column($tmp_result, $value);
		} else {
			foreach ($tmp_result as $cval) {
				$result[$cval[$key]] = $cval[$value];
			}
		}

		return $result;
	}

	/**
	 * Returns auto increment ID, which was inserted into DB in the last time
	 *
	 * @return int
	 */
	public function last_insert_id()
	{
		return $this->current_connection->lastInsertId();
	}

	/**
	 * Clean / destroy connections
	 *
	 * @return void
	 **/
	static public function clean($connection_name)
	{
		if ($connection_name) {
			if (isset(self::$connections[$connection_name])) {
				unset(self::$connections_config[$connection_name], self::$connections[$connection_name]);
			}
		} else {
			self::$connections_config = [];
			self::$connections = FALSE;
		}
	}
}
