<?php
/**
 * Core class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 **/

namespace uDB;

use \uDB, \_InvalidArgumentException;

class DBDump
{
	private $table_name = '';
	private $rows = [];
	private $sql_statement = FALSE;
	private $statement_method = FALSE;
	private $where = '';

	static public function init($table_name, array $rows, $statement)
	{
		if (!is_string($table_name) || empty($table_name)) {
			throw new _InvalidArgumentException('Invalid $table_name');
		}

		if (!is_array($rows)) {
			throw new \_InvalidArgumentException('Array $rows is invalid');
		}

		switch ($statement) {
			case uDB::TYPE_INSERT:
				$statement_method = 'with_insert';
			break;

			case uDB::TYPE_INSERT_IGNORE:
				$statement_method = 'with_insert_ignore';
			break;

			case uDB::TYPE_UPDATE:
				$statement_method = 'with_update';
			break;

			case uDB::TYPE_REPLACE:
				$statement_method = 'with_replace';
			break;

			default:
				throw new _InvalidArgumentException('Not allowed value of $sql_type');
		}

		$object = new self();
		$object->rows = $rows;
		$object->statement_method = $statement_method;
		$object->sql_statement = $statement;

		$table_name = str_replace('`', '', $table_name);
		$table_name = explode('.', $table_name);

		$object->table_name = '`' . implode('`.`', $table_name) . '`';

		return $object;
	}

	private function __construct() {}

	/**
	 * @param string $conditions
	 *
	 * @return $this
	 */
	public function where($conditions = '')
	{
		if (!is_string($conditions)) {
			throw new _InvalidArgumentException('Invalid $conditions. In must be string');
		}

		if (!empty($conditions)) {
			$this->where = "\n WHERE {$conditions} ";
		}

		return $this;
	}

	/**
	 * @return string
	 */
	public function execute()
	{
		if (!$this->statement_method) {
			throw new _InvalidArgumentException('Invalid statement_method');
		}

		$statement_method = $this->statement_method;

		return $this->$statement_method();
	}

	/**
	 * @param array $binds_array
	 * @return string
	 */
	private function binds(array $binds_array)
	{
		if (empty($binds_array) && !is_array($binds_array)) {
			throw new _InvalidArgumentException('Invalid $binds_array');
		}

		if (!is_string($this->where)) {
			throw new _InvalidArgumentException('Invalid "WHERE" conditions');
		}

		$where = $this->where;

		if ($where) {
			foreach ($binds_array as $col_name => $col_value) {
				if (!is_scalar($col_value) && !is_null($col_value) && !is_bool($col_value)) {
					throw new _InvalidArgumentException('Invalid $col_value. It must be scalar, NULL or bool');
				}

				$where = str_replace(":{$col_name}", "'{$col_value}'", $where);
			}
		}

		return $where;
	}

	/**
	 * @return string
	 */
	private function with_update()
	{
		$result = '';

		foreach ($this->rows as $row) {
			$columns = [];

			foreach ($row as $col_name => $col_value) {
				if (strtolower($col_value) == 'null' || is_null($col_value)) {
					$col_value = 'NULL';
				} else {
					$col_value = "'" . str_replace("'", "\'", $col_value) . "'";
				}

				$columns[] = " `{$col_name}` = {$col_value}";
			}

			$columns = implode(',', $columns);

			$result .=
				" UPDATE {$this->table_name} \n" .
				" SET {$columns} " .
				$this->binds($row) . ";\n\n";
		}

		return $result;
	}

	/**
	 * @param bool $ignore
	 * @return string
	 */
	private function with_insert($ignore = '')
	{
		if ($ignore) {
			$ignore = ' IGNORE ';
		}

		$columns
		= $create_collumn
		= $result
		= '';


		foreach ($this->rows as $row) {
			$column_names
			= $col_values
			= [];

			foreach ($row as $col_name => $col_value) {
				if (!$columns) {
					if (strtotime($col_value) > 0) {
						$column_type = 'DATETIME NULL';
					} elseif (strlen($col_value) > 256) {
						$column_type = 'TEXT';
					} elseif (strlen((int)$col_value) != strlen($col_value)) {
						$column_type = 'VARCHAR(256) ';
					} else {
						$column_type = 'INT(11) NULL';
					}

					$create_collumn[] = "`{$col_name}` {$column_type}";
					$column_names[] = $col_name;
				}


				if (strtolower($col_value) == 'null' || is_null($col_value)) {
					$col_value = 'NULL';
				} else {
					$col_value = "'" . str_replace("'", "\'", $col_value) . "'";
				}

				$col_values[] = $col_value;
			}

			if (!$columns) {
				$create_collumn = implode(", \n", $create_collumn);
				$columns = implode(', ', $column_names);
			}

			$col_values = implode(',', $col_values);

			$result .= "({$col_values}),\n";
		}

		if (substr(trim($result), -1) == ",") {
			$result = substr(trim($result), 0, -1);
		}

		return "CREATE TABLE IF NOT EXISTS {$this->table_name}\n(\n{$create_collumn}\n);\n\n" .
 			"INSERT {$ignore} INTO {$this->table_name} \n ({$columns}) \n VALUES \n{$result};";
	}

	/**
	 * @return string
	 */
	private function with_insert_ignore()
	{
		return $this->with_insert(TRUE);
	}

	/**
	 * @return string
	 */
	private function with_replace()
	{
		throw new \_InvalidArgumentException('Method was not completed');
	}
}
