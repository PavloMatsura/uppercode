<?php
/**
 * Class uBugtracker
 *
 *   HOW To USE
 * ==============
 *
 * ## Use method `deprecated()` to catch legacy code
 * uBugtracker::init()
 *  ->deprecated();
 *
 * ## Use method `add_track()` to catch throwed message
 * uBugtracker::init()
 *  ->add_track(\Exception('Some message'));
 */
class uBugtracker
{
	/**
	 * @var static
	 */
	static private $bugtracker;

	/**
	 * @return static
	 */
	static public function init()
	{
		if (!(self::$bugtracker instanceof static)) {
			$basic_config = require APP_ROUTE . 'basic_config.php';

			if (!isset($basic_config['bugtracker_class'])) {
				self::$bugtracker = new self();
			} elseif (is_string($basic_config['bugtracker_class']) && $basic_config['bugtracker_class'] !== '') {
				self::$bugtracker = new $basic_config['bugtracker_class']();
			} else {
				___dump('Settings at ' . APP_ROUTE . 'basic_config.php -> \'bugtracker_class\' is invalid ',1,1);
			}
		}

		return self::$bugtracker;
	}

	/**
	 * Uses to define deprecated function/methods/other things
	 * @example  \uBugtracker()->deprecated()
	 */
	public function deprecated()
	{
		___dump((new \Exception('Deprecated'))->getTrace());
	}

	/**
	 * Uses to tracks some issues. E.g. throwed message
	 *
	 * @important this method has not to throw any exceptions
	 */
	public function add_track(\Exception $e, $tracked_data = [])
	{
		if (DEBUG_MODE === true) {
			___dump([$e, 'tracked_data' => $tracked_data],'uBugtracker',1);
		}
	}
}
