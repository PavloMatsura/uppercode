<?php
/**
 * Core sub class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013-2017 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 **/

namespace sub_classes;

use \_InvalidArgumentException;

class validation
{
	private $messages = [];
	static private $allowed_params = [
		'type',
		'max_length',
		'min_length',
		'max_value',
		'min_value',
		'required_message',
		'error_message',
	];

	/**
	 * @param array $params
	 * [
	 * 		'required_message' => scalar, //Set required_message string to make value required. This message will showed, if $data_value is false|empty. Set false or drop out this param, if data is not required
	 * 		'error_message' => string, //Not required. This value will returned, if $data_value is invalid, according to `type`, `length` and `value`. If `error_message` is empty and `required_message` is not empty - will be returned `required_message` as error mesasge.
	 * 		'type' => int|float|ip|email|string|array|object|null|bool|regexp, //Required parameter. Validate $data_value type or regexp string
	 * 		'max_length' => int, //max length of $data_value, set 0 or left this param, if no needs to validate $data_value by this condition
	 * 		'min_length' => int, //min length of $data_value, set 0 or left this param, if no needs to validate $data_value by this condition
	 * 		'max_value' => int, //max value of numeric value, set 0 or drop out this param, if no needs to validate $data_value by this condition
	 * 		'min_value' => int, //min value of numeric value, set NULL or drop out this param, if no needs to validate $data_value by this condition
	 * ]
	 *
	 * @return validation
	 **/
	public function value($data_value, $params)
	{
		if (is_scalar($data_value)) {
			$data_value = trim($data_value);
		}

		if (!is_string($params['type']) || empty($params['type'])) {
			throw new _InvalidArgumentException('Invalid value of parameter "type"');
		}

		if (empty($params['error_message']) && empty($params['required_message'])) {
			throw new _InvalidArgumentException('Needs to set "error_message" or "required_message" parameter');
		}

		if (!empty($params['required_message']) && !is_string($params['required_message'])) {
			throw new _InvalidArgumentException('Parameter "required_message" must be nor empty string');
		}

		if (!empty($params['error_message']) && !is_string($params['error_message'])) {
			throw new _InvalidArgumentException('Parameter "error_message" must be nor empty string');
		}


		$data = [];

		foreach (self::$allowed_params as $key) {
			$data[$key] = NULL;

			if (isset($params[$key])) {
				$data[$key] = $params[$key];
			}
		}

		if (empty($data_value) && !(bool)$data['required_message']) {
			return $this;
		} elseif (empty($data_value)) {
			$this->messages[] = $data['required_message'];
		}

		if (is_null($data['error_message'])) {
			$data['error_message'] = '';
		}

		if (
			!self::check_data_type($data_value, $data['type'])
			|| (
				!in_array($data['type'], ['int', 'integer', 'float'])
				&& !self::check_data_length($data_value, $data['max_length'], $data['min_length'])
			)
			|| (
				in_array($data['type'], ['int', 'integer', 'float'])
				&& !self::check_data_value($data_value, $data['max_value'], $data['min_value'], $data)
			)
		) {
			if (empty($data['error_message']) && !empty($data['required_message'])) {
				$data['error_message'] = $data['required_message'];
			}

			$this->messages[] = $data['error_message'];
		}

		return $this;
	}

	/**
	 * Returns messages array
	 *
	 * @return array
	 **/
	public function execute()
	{
		return $this->messages;
	}

	static private function check_data_type($data_value, $type)
	{
		switch (strtolower($type)) {
			case 'int': case 'integer':
				$result = is_int(filter_var($data_value, FILTER_VALIDATE_INT));
			break;

			case 'float':
				$result = is_float(filter_var($data_value, FILTER_VALIDATE_FLOAT));
			break;

			case 'ip':
				$result = filter_var($data_value, FILTER_VALIDATE_IP);
			break;

			case 'email':
				$result = filter_var($data_value, FILTER_VALIDATE_EMAIL);
			break;

			case 'string':
				$result = is_string($data_value);
			break;

			case 'array':
				$result = is_array($data_value);
			break;

			case 'object':
				$result = is_object($data_value);
			break;

			case 'null':
				$result = is_null($data_value);
			break;

			case 'bool': case 'boolean':
				$result = is_bool($data_value);
			break;

			default:
				$result = filter_var($data_value, FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => "/{$type}{" . strlen($data_value) . "}/"]]);
			break;
		}

		return $result;
	}

	static private function check_data_length($data_value, $max_length, $min_length)
	{
		$return = true;

		if (($max_length > 0 && strlen($data_value) > $max_length) || ($min_length > 0 && strlen($data_value) < $min_length)){
			$return = false;
		}

		return $return;
	}

	static private function check_data_value($data_value, $max_value, $min_value, $data)
	{
		$return = true;

		if ((!is_null($max_value) && $data_value > $max_value) || (!is_null($min_value) && $data_value < $min_value)){
			$return = false;
		}

		return $return;
	}
}