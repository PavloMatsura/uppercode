<?php
namespace Twig;


class AppExtension extends \Twig_Extension
{
	public function getFunctions()
	{
		return [
			new \Twig_Function('_l', '_l'),
			new \Twig_Function('_lang','_l'),
		];
	}

	public function getFilters()
	{
		return [
			new \Twig_Filter('html', [$this, 'htmlFilter']),
		];
	}

	public function htmlFilter($string_args)
	{
		echo $string_args;
	}
}
