<?php
/**
 * Core class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 **/

/**
 * Class uDates
 *
 * Main class for works with dates
 *
 */
class uDates {
	const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s'; //default date_format

	static protected $time_zone = NULL;
	static protected $date_format = self::DEFAULT_DATE_FORMAT;

	static public function init()
	{
		return \uDates\methods::init(static::$date_format);
	}

	static public function dateFormat($date_format)
	{
		if (!is_string($date_format) || empty($date_format)) {
			throw new _InvalidArgumentException('Invalid $date_format');
		}

		static::$date_format = $date_format;
	}

	/**
	 * @param bool|string|int $timestamp
	 *
	 * @return string
	 */
	static public function getDate($timestamp = FALSE)
	{
		return \uDates\methods::init(static::$date_format)
			->timeZone(static::$time_zone)
			->getDate($timestamp);
	}

	/**
	 * @param $time_zone
	 */
	static public function timeZone($time_zone)
	{
		if (is_string($time_zone) && !self::timeZoneValidation($time_zone)) {
			throw new _InvalidArgumentException('Invalid $time_zone');
		}

		if (!is_string($time_zone)) {
			$time_zone = ini_get('date.timezone');
		}

		return static::$time_zone = new DateTimeZone($time_zone);
	}

	/**
	 * @return \DateTime
	 */
	static public function getObject()
	{
		return self::init()
			->getObject();
	}

	/**
	 * @param $time_zone
	 */
	static public function timeZoneValidation($time_zone)
	{
		switch ($time_zone) {
			case 'Africa/Abidjan':
			case 'Africa/Accra':
			case 'Africa/Addis_Ababa':
			case 'Africa/Algiers':
			case 'Africa/Asmara':
			case 'Africa/Asmera':
			case 'Africa/Bamako':
			case 'Africa/Bangui':
			case 'Africa/Banjul':
			case 'Africa/Bissau':
			case 'Africa/Blantyre':
			case 'Africa/Brazzaville':
			case 'Africa/Bujumbura':
			case 'Africa/Cairo':
			case 'Africa/Casablanca':
			case 'Africa/Ceuta':
			case 'Africa/Conakry':
			case 'Africa/Dakar':
			case 'Africa/Dar_es_Salaam':
			case 'Africa/Djibouti':
			case 'Africa/Douala':
			case 'Africa/El_Aaiun':
			case 'Africa/Freetown':
			case 'Africa/Gaborone':
			case 'Africa/Harare':
			case 'Africa/Johannesburg':
			case 'Africa/Juba':
			case 'Africa/Kampala':
			case 'Africa/Khartoum':
			case 'Africa/Kigali':
			case 'Africa/Kinshasa':
			case 'Africa/Lagos':
			case 'Africa/Libreville':
			case 'Africa/Lome':
			case 'Africa/Luanda':
			case 'Africa/Lubumbashi':
			case 'Africa/Lusaka':
			case 'Africa/Malabo':
			case 'Africa/Maputo':
			case 'Africa/Maseru':
			case 'Africa/Mbabane':
			case 'Africa/Mogadishu':
			case 'Africa/Monrovia':
			case 'Africa/Nairobi':
			case 'Africa/Ndjamena':
			case 'Africa/Niamey':
			case 'Africa/Nouakchott':
			case 'Africa/Ouagadougou':
			case 'Africa/Porto-Novo':
			case 'Africa/Sao_Tome':
			case 'Africa/Timbuktu':
			case 'Africa/Tripoli':
			case 'Africa/Tunis':
			case 'Africa/Windhoek':
			case 'America/Adak':
			case 'America/Anchorage':
			case 'America/Anguilla':
			case 'America/Antigua':
			case 'America/Araguaina':
			case 'America/Argentina/Buenos_Aires':
			case 'America/Argentina/Catamarca':
			case 'America/Argentina/ComodRivadavia':
			case 'America/Argentina/Cordoba':
			case 'America/Argentina/Jujuy':
			case 'America/Argentina/La_Rioja':
			case 'America/Argentina/Mendoza':
			case 'America/Argentina/Rio_Gallegos':
			case 'America/Argentina/Salta':
			case 'America/Argentina/San_Juan':
			case 'America/Argentina/San_Luis':
			case 'America/Argentina/Tucuman':
			case 'America/Argentina/Ushuaia':
			case 'America/Aruba':
			case 'America/Asuncion':
			case 'America/Atikokan':
			case 'America/Atka':
			case 'America/Bahia':
			case 'America/Bahia_Bandera':
			case 'America/Barbados':
			case 'America/Belem':
			case 'America/Belize':
			case 'America/Blanc-Sablon':
			case 'America/Boa_Vista':
			case 'America/Bogota':
			case 'America/Boise':
			case 'America/Buenos_Aires':
			case 'America/Cambridge_Bay':
			case 'America/Campo_Grande':
			case 'America/Cancun':
			case 'America/Caracas':
			case 'America/Catamarca':
			case 'America/Cayenne':
			case 'America/Cayman':
			case 'America/Chicago':
			case 'America/Chihuahua':
			case 'America/Coral_Harbour':
			case 'America/Cordoba':
			case 'America/Costa_Rica':
			case 'America/Creston':
			case 'America/Cuiaba':
			case 'America/Curacao':
			case 'America/Danmarkshavn':
			case 'America/Dawson':
			case 'America/Dawson_Creek':
			case 'America/Denver':
			case 'America/Detroit':
			case 'America/Dominica':
			case 'America/Edmonton':
			case 'America/Eirunepe':
			case 'America/El_Salvador':
			case 'America/Ensenada':
			case 'America/Fort_Nelson':
			case 'America/Fort_Wayne':
			case 'America/Fortaleza':
			case 'America/Glace_Bay':
			case 'America/Godthab':
			case 'America/Goose_Bay':
			case 'America/Grand_Turk':
			case 'America/Grenada':
			case 'America/Guadeloupe':
			case 'America/Guatemala':
			case 'America/Guayaquil':
			case 'America/Guyana':
			case 'America/Halifax':
			case 'America/Havana':
			case 'America/Hermosillo':
			case 'America/Indiana/Indianapolis':
			case 'America/Indiana/Knox':
			case 'America/Indiana/Marengo':
			case 'America/Indiana/Petersburg':
			case 'America/Indiana/Tell_City':
			case 'America/Indiana/Vevay':
			case 'America/Indiana/Vincennes':
			case 'America/Indiana/Winamac':
			case 'America/Indianapolis':
			case 'America/Inuvik':
			case 'America/Iqaluit':
			case 'America/Jamaica':
			case 'America/Jujuy':
			case 'America/Juneau':
			case 'America/Kentucky/Louisville':
			case 'America/Kentucky/Monticello':
			case 'America/Knox_IN':
			case 'America/Kralendijk':
			case 'America/La_Paz':
			case 'America/Lima':
			case 'America/Los_Angeles':
			case 'America/Louisville':
			case 'America/Lower_Princes':
			case 'America/Maceio':
			case 'America/Managua':
			case 'America/Manaus':
			case 'America/Marigot':
			case 'America/Martinique':
			case 'America/Matamoros':
			case 'America/Mazatlan':
			case 'America/Mendoza':
			case 'America/Menominee':
			case 'America/Merida':
			case 'America/Metlakatla':
			case 'America/Mexico_City':
			case 'America/Miquelon':
			case 'America/Moncton':
			case 'America/Monterrey':
			case 'America/Montevideo':
			case 'America/Montreal':
			case 'America/Montserrat':
			case 'America/Nassau':
			case 'America/New_York':
			case 'America/Nipigon':
			case 'America/Nome':
			case 'America/Noronha':
			case 'America/North_Dakota/Beulah':
			case 'America/North_Dakota/Center':
			case 'America/North_Dakota/New_Salem':
			case 'America/Ojinaga':
			case 'America/Panama':
			case 'America/Pangnirtung':
			case 'America/Paramaribo':
			case 'America/Phoenix':
			case 'America/Port-au':
			case 'America/Port_of_Spain':
			case 'America/Porto_Acre':
			case 'America/Porto_Velho':
			case 'America/Puerto_Rico':
			case 'America/Punta_Arenas':
			case 'America/Rainy_River':
			case 'America/Rankin_Inlet':
			case 'America/Recife':
			case 'America/Regina':
			case 'America/Resolute':
			case 'America/Rio_Branco':
			case 'America/Rosario':
			case 'America/Santa_Isabel':
			case 'America/Santarem':
			case 'America/Santiago':
			case 'America/Santo_Domingo':
			case 'America/Sao_Paulo':
			case 'America/Scoresbysund':
			case 'America/Shiprock':
			case 'America/Sitka':
			case 'America/St_Barthelemy':
			case 'America/St_Johns':
			case 'America/St_Kitts':
			case 'America/St_Lucia':
			case 'America/St_Thomas':
			case 'America/St_Vincent':
			case 'America/Swift_Current':
			case 'America/Tegucigalpa':
			case 'America/Thule':
			case 'America/Thunder_Bay':
			case 'America/Tijuana':
			case 'America/Toronto':
			case 'America/Tortola':
			case 'America/Vancouver':
			case 'America/Virgin':
			case 'America/Whitehorse':
			case 'America/Winnipeg':
			case 'America/Yakutat':
			case 'America/Yellowknife':
			case 'Antarctica/Casey':
			case 'Antarctica/Davis':
			case 'Antarctica/DumontDUrv':
			case 'Antarctica/Macquarie':
			case 'Antarctica/Mawson':
			case 'Antarctica/McMurdo':
			case 'Antarctica/Palmer':
			case 'Antarctica/Rothera':
			case 'Antarctica/South_Pole':
			case 'Antarctica/Syowa':
			case 'Antarctica/Troll':
			case 'Antarctica/Vostok':
			case 'Arctic/Longyearbyen':
			case 'Asia/Aden':
			case 'Asia/Almaty':
			case 'Asia/Amman':
			case 'Asia/Anadyr':
			case 'Asia/Aqtau':
			case 'Asia/Aqtobe':
			case 'Asia/Ashgabat':
			case 'Asia/Ashkhabad':
			case 'Asia/Atyrau':
			case 'Asia/Baghdad':
			case 'Asia/Bahrain':
			case 'Asia/Baku':
			case 'Asia/Bangkok':
			case 'Asia/Barnaul':
			case 'Asia/Beirut':
			case 'Asia/Bishkek':
			case 'Asia/Brunei':
			case 'Asia/Calcutta':
			case 'Asia/Chita':
			case 'Asia/Choibalsan':
			case 'Asia/Chongqing':
			case 'Asia/Chungking':
			case 'Asia/Colombo':
			case 'Asia/Dacca':
			case 'Asia/Damascus':
			case 'Asia/Dhaka':
			case 'Asia/Dili':
			case 'Asia/Dubai':
			case 'Asia/Dushanbe':
			case 'Asia/Famagusta':
			case 'Asia/Gaza':
			case 'Asia/Harbin':
			case 'Asia/Hebron':
			case 'Asia/Ho_Chi_Minh':
			case 'Asia/Hong_Kong':
			case 'Asia/Hovd':
			case 'Asia/Irkutsk':
			case 'Asia/Istanbul':
			case 'Asia/Jakarta':
			case 'Asia/Jayapura':
			case 'Asia/Jerusalem':
			case 'Asia/Kabul':
			case 'Asia/Kamchatka':
			case 'Asia/Karachi':
			case 'Asia/Kashgar':
			case 'Asia/Kathmandu':
			case 'Asia/Katmandu':
			case 'Asia/Khandyga':
			case 'Asia/Kolkata':
			case 'Asia/Krasnoyarsk':
			case 'Asia/Kuala_Lumpur':
			case 'Asia/Kuching':
			case 'Asia/Kuwait':
			case 'Asia/Macao':
			case 'Asia/Macau':
			case 'Asia/Magadan':
			case 'Asia/Makassar':
			case 'Asia/Manila':
			case 'Asia/Muscat':
			case 'Asia/Nicosia':
			case 'Asia/Novokuznetsk':
			case 'Asia/Novosibirsk':
			case 'Asia/Omsk':
			case 'Asia/Oral':
			case 'Asia/Phnom_Penh':
			case 'Asia/Pontianak':
			case 'Asia/Pyongyang':
			case 'Asia/Qatar':
			case 'Asia/Qyzylorda':
			case 'Asia/Rangoon':
			case 'Asia/Riyadh':
			case 'Asia/Saigon':
			case 'Asia/Sakhalin':
			case 'Asia/Samarkand':
			case 'Asia/Seoul':
			case 'Asia/Shanghai':
			case 'Asia/Singapore':
			case 'Asia/Srednekolymsk':
			case 'Asia/Taipei':
			case 'Asia/Tashkent':
			case 'Asia/Tbilisi':
			case 'Asia/Tehran':
			case 'Asia/Tel_Aviv':
			case 'Asia/Thimbu':
			case 'Asia/Thimphu':
			case 'Asia/Tokyo':
			case 'Asia/Tomsk':
			case 'Asia/Ujung_Pandang':
			case 'Asia/Ulaanbaatar':
			case 'Asia/Ulan_Bator':
			case 'Asia/Urumqi':
			case 'Asia/Ust-Nera':
			case 'Asia/Vientiane':
			case 'Asia/Vladivostok':
			case 'Asia/Yakutsk':
			case 'Asia/Yangon':
			case 'Asia/Yekaterinburg':
			case 'Asia/Yerevan':
			case 'Atlantic/Azores':
			case 'Atlantic/Bermuda':
			case 'Atlantic/Canary':
			case 'Atlantic/Cape_Verde':
			case 'Atlantic/Faeroe':
			case 'Atlantic/Faroe':
			case 'Atlantic/Jan_Mayen':
			case 'Atlantic/Madeira':
			case 'Atlantic/Reykjavik':
			case 'Atlantic/South_Georgi':
			case 'Atlantic/St_Helena':
			case 'Atlantic/Stanley':
			case 'Australia/ACT':
			case 'Australia/Adelaide':
			case 'Australia/Brisbane':
			case 'Australia/Broken_Hill':
			case 'Australia/Canberra':
			case 'Australia/Currie':
			case 'Australia/Darwin':
			case 'Australia/Eucla':
			case 'Australia/Hobart':
			case 'Australia/LHI':
			case 'Australia/Lindeman':
			case 'Australia/Lord_Howe':
			case 'Australia/Melbourne':
			case 'Australia/North':
			case 'Australia/NSW':
			case 'Australia/Perth':
			case 'Australia/Queensland':
			case 'Australia/South':
			case 'Australia/Sydney':
			case 'Australia/Tasmania':
			case 'Australia/Victoria':
			case 'Australia/West':
			case 'Australia/Yancowinna':
			case 'Brazil/Acre':
			case 'Brazil/DeNoronha':
			case 'Brazil/East':
			case 'Brazil/West':
			case 'Canada/Atlantic':
			case 'Canada/Central':
			case 'Canada/Eastern':
			case 'Canada/Mountain':
			case 'Canada/Newfoundland':
			case 'Canada/Pacific':
			case 'Canada/Saskatchewan':
			case 'Canada/Yukon':
			case 'CET':
			case 'Chile/Continental':
			case 'Chile/EasterIsland':
			case 'CST6CDT':
			case 'Cuba':
			case 'EET':
			case 'Egypt':
			case 'Eire':
			case 'EST':
			case 'EST5EDT':
			case 'Etc/GMT':
			case 'Etc/GMT+0':
			case 'Etc/GMT+1':
			case 'Etc/GMT+10':
			case 'Etc/GMT+11':
			case 'Etc/GMT+12':
			case 'Etc/GMT+2':
			case 'Etc/GMT+3':
			case 'Etc/GMT+4':
			case 'Etc/GMT+5':
			case 'Etc/GMT+6':
			case 'Etc/GMT+7':
			case 'Etc/GMT+8':
			case 'Etc/GMT+9':
			case 'Etc/GMT-0':
			case 'Etc/GMT-1':
			case 'Etc/GMT-10':
			case 'Etc/GMT-11':
			case 'Etc/GMT-12':
			case 'Etc/GMT-13':
			case 'Etc/GMT-14':
			case 'Etc/GMT-2':
			case 'Etc/GMT-3':
			case 'Etc/GMT-4':
			case 'Etc/GMT-5':
			case 'Etc/GMT-6':
			case 'Etc/GMT-7':
			case 'Etc/GMT-8':
			case 'Etc/GMT-9':
			case 'Etc/GMT0':
			case 'Etc/Greenwich':
			case 'Etc/UCT':
			case 'Etc/Universal':
			case 'Etc/UTC':
			case 'Etc/Zulu':
			case 'Europe/Amsterdam':
			case 'Europe/Andorra':
			case 'Europe/Astrakhan':
			case 'Europe/Athens':
			case 'Europe/Belfast':
			case 'Europe/Belgrade':
			case 'Europe/Berlin':
			case 'Europe/Bratislava':
			case 'Europe/Brussels':
			case 'Europe/Bucharest':
			case 'Europe/Budapest':
			case 'Europe/Busingen':
			case 'Europe/Chisinau':
			case 'Europe/Copenhagen':
			case 'Europe/Dublin':
			case 'Europe/Gibraltar':
			case 'Europe/Guernsey':
			case 'Europe/Helsinki':
			case 'Europe/Isle_of_Man':
			case 'Europe/Istanbul':
			case 'Europe/Jersey':
			case 'Europe/Kaliningrad':
			case 'Europe/Kiev':
			case 'Europe/Kirov':
			case 'Europe/Lisbon':
			case 'Europe/Ljubljana':
			case 'Europe/London':
			case 'Europe/Luxembourg':
			case 'Europe/Madrid':
			case 'Europe/Malta':
			case 'Europe/Mariehamn':
			case 'Europe/Minsk':
			case 'Europe/Monaco':
			case 'Europe/Moscow':
			case 'Europe/Nicosia':
			case 'Europe/Oslo':
			case 'Europe/Paris':
			case 'Europe/Podgorica':
			case 'Europe/Prague':
			case 'Europe/Riga':
			case 'Europe/Rome':
			case 'Europe/Samara':
			case 'Europe/San_Marino':
			case 'Europe/Sarajevo':
			case 'Europe/Saratov':
			case 'Europe/Simferopol':
			case 'Europe/Skopje':
			case 'Europe/Sofia':
			case 'Europe/Stockholm':
			case 'Europe/Tallinn':
			case 'Europe/Tirane':
			case 'Europe/Tiraspol':
			case 'Europe/Ulyanovsk':
			case 'Europe/Uzhgorod':
			case 'Europe/Vaduz':
			case 'Europe/Vatican':
			case 'Europe/Vienna':
			case 'Europe/Vilnius':
			case 'Europe/Volgograd':
			case 'Europe/Warsaw':
			case 'Europe/Zagreb':
			case 'Europe/Zaporozhye':
			case 'Europe/Zurich':
			case 'Factory':
			case 'GB':
			case 'GB-Eire':
			case 'GMT':
			case 'GMT+0':
			case 'GMT-0':
			case 'GMT0':
			case 'Greenwich':
			case 'Hongkong':
			case 'HST':
			case 'Iceland':
			case 'Indian/Antananarivo':
			case 'Indian/Chagos':
			case 'Indian/Christmas':
			case 'Indian/Cocos':
			case 'Indian/Comoro':
			case 'Indian/Kerguelen':
			case 'Indian/Mahe':
			case 'Indian/Maldives':
			case 'Indian/Mauritius':
			case 'Indian/Mayotte':
			case 'Indian/Reunion':
			case 'Iran':
			case 'Israel':
			case 'Jamaica':
			case 'Japan':
			case 'Kwajalein':
			case 'Libya':
			case 'MET':
			case 'Mexico/BajaNorte':
			case 'Mexico/BajaSur':
			case 'Mexico/General':
			case 'MST':
			case 'MST7MDT':
			case 'Navajo':
			case 'NZ':
			case 'NZ-CHAT':
			case 'Pacific/Apia':
			case 'Pacific/Auckland':
			case 'Pacific/Bougainville':
			case 'Pacific/Chatham':
			case 'Pacific/Chuuk':
			case 'Pacific/Easter':
			case 'Pacific/Efate':
			case 'Pacific/Enderbury':
			case 'Pacific/Fakaofo':
			case 'Pacific/Fiji':
			case 'Pacific/Funafuti':
			case 'Pacific/Galapagos':
			case 'Pacific/Gambier':
			case 'Pacific/Guadalcanal':
			case 'Pacific/Guam':
			case 'Pacific/Honolulu':
			case 'Pacific/Johnston':
			case 'Pacific/Kiritimati':
			case 'Pacific/Kosrae':
			case 'Pacific/Kwajalein':
			case 'Pacific/Majuro':
			case 'Pacific/Marquesas':
			case 'Pacific/Midway':
			case 'Pacific/Nauru':
			case 'Pacific/Niue':
			case 'Pacific/Norfolk':
			case 'Pacific/Noumea':
			case 'Pacific/Pago_Pago':
			case 'Pacific/Palau':
			case 'Pacific/Pitcairn':
			case 'Pacific/Pohnpei':
			case 'Pacific/Ponape':
			case 'Pacific/Port_Moresby':
			case 'Pacific/Rarotonga':
			case 'Pacific/Saipan':
			case 'Pacific/Samoa':
			case 'Pacific/Tahiti':
			case 'Pacific/Tarawa':
			case 'Pacific/Tongatapu':
			case 'Pacific/Truk':
			case 'Pacific/Wake':
			case 'Pacific/Wallis':
			case 'Pacific/Yap':
			case 'Poland':
			case 'Portugal':
			case 'PRC':
			case 'PST8PDT':
			case 'ROC':
			case 'ROK':
			case 'Singapore':
			case 'Turkey':
			case 'UCT':
			case 'Universal':
			case 'US/Alaska':
			case 'US/Aleutian':
			case 'US/Arizona':
			case 'US/Central':
			case 'US/East-Indiana':
			case 'US/Eastern':
			case 'US/Hawaii':
			case 'US/Indiana-Starke':
			case 'US/Michigan':
			case 'US/Mountain':
			case 'US/Pacific':
			case 'US/Pacific-New':
			case 'US/Samoa':
			case 'UTC':
			case 'W-SU':
			case 'WET':
			case 'Zulu':
				return TRUE;
			break;

			default:
				return FALSE;
		}
	}
}
