<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 * Exceptionі types
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

class uExceptions extends Exception
{
	/**@deprecated */
	public static function init(){
		uBugtracker::init()->deprecated();
	}
}

class _InvalidArgumentException extends LogicException
{
	/**
	 * @deprecated
	 * _InvalidArgumentException constructor.
	 *
	 * @param string $message
	 * @param int $code
	 * @param Throwable|null $previous
	 */
	public function __construct($message = "", $code = 0, Throwable $previous = null) {
		uBugtracker::init()->deprecated();
		parent::__construct($message, $code, $previous);
	}
}

class _RuntimeException extends RuntimeException
{
	/**@deprecated */
	public function __construct($message = "", $code = 0, Throwable $previous = null) {
		uBugtracker::init()->deprecated();
		parent::__construct($message, $code, $previous);
	}
}

class _FrontSideException extends Exception
{
	/**@deprecated */
	public function __construct($message = "", $code = 0, Throwable $previous = null) {
		uBugtracker::init()->deprecated();
		parent::__construct($message, $code, $previous);
	}
}
