<?php
/**
 * Core class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 **/
/**
 * Main API class
 */
class uAPI
{
	const PAGE_TYPE = 'api';

	static public $URL_rules = 'api/[api]';

	static private $allowed = [];
	static private $objects;

	static public function init()
	{
		$error_line = '';
		$called_methods = [];
		$method_param = PData::_GET('api');

		if (!empty(self::$allowed[CANONICAL_PAGE_URL]) && is_array(self::$allowed[CANONICAL_PAGE_URL])) {
			foreach (self::$allowed[CANONICAL_PAGE_URL] as $class_name => $methods) {
				foreach ($methods as $method_name => $details) {
					if (class_exists($details['namespace'])) {
						if (!$details['is_dynamic_method']) { //Static calling
							if (method_exists($details['namespace'], $method_name)) {
								$details['namespace']::$method_name($method_param);
							} else {
								$called_methods[] = "{$details['namespace']}::{$method_name}({$method_param})";
								$error_line = __LINE__;
							}
						} else { //Dynamic calling
							self::$objects = new $details['namespace']();

							if (method_exists(self::$objects, $method_name)) {
								self::$objects->$method_name($method_param);
							} else {
								$called_methods[] = "{$details['namespace']}->{$method_name}({$method_param})";
								$error_line = __LINE__;
							}
						}
					}
				}
			}
		} else {
			$called_methods[] = "No methods defined for " . CANONICAL_PAGE_URL;
			$error_line = __LINE__;
		}

		if (!empty($error_line)) {
			PData::setMessage(['code' => -1, 'status' => 'error', 'message' => 'Some methods does not exists for ' . CANONICAL_PAGE_URL . ' #' . $error_line]);

			if (defined('SAVE_LOG') && SAVE_LOG) {
				uCode::saveLog(['CANONICAL_PAGE_URL' => CANONICAL_PAGE_URL, 'allowed_array' => self::$allowed],'not_exists_api_methods');
			}
		}
	}

	/**
	 * Set class and method to allowed method list
	 *
	 * @param string $api_CANONICAL_PAGE_URL
	 * @param string $namespace - namespace part and class name part: uCode(), or mod\example_module()
	 * @param string $method_name
	 * @param bool $is_object_method - method call type (FALSE: static; TRUE: dynamic) (default: FALSE)
	 *
	 * @example
	 *          uAPI::allow_method(_getURL(uAPI::PAGE_TYPE, 'post.js'), 'mod\example_module', 'stat_meth');
	 *          // will allow to call mod\example_module::stat_meth('post.js') if CANONICAL_PAGE_URL = 'http://mywebsite.com/api/post.js'
	 *
	 * @return void
	 *
	 * @throws _InvalidArgumentException
	 * 			if $class_name empty or not string
	 * 			if $method_name empty or not string
	 * 			if $is_dynamic_method is not bool
	 *          if returned class $class_name is already allowed
	 */
	static public function allow_method($api_CANONICAL_PAGE_URL, $namespace, $method_name, $is_object_method = FALSE)
	{
		if (empty($namespace) && !is_string($namespace)) {
			throw new InvalidArgumentException('$namespace must be not empty string');
		}

		if (empty($method_name) && !is_string($method_name)) {
			throw new InvalidArgumentException('$method_name must be not empty string');
		}

		if (!is_bool($is_object_method)) {
			throw new InvalidArgumentException('$is_dynamic_method must be boolean');
		}

		$tmp_array = explode('\\', $namespace);
		$class_name = $tmp_array[count($tmp_array) - 1];

		if (isset(self::$allowed[$class_name][$method_name])) {
			$fragment = ($is_object_method ? '->' : '::');

			throw new InvalidArgumentException("{$namespace}{$fragment}{$method_name}() already allowed");
		}

		self::$allowed[$api_CANONICAL_PAGE_URL][$class_name][$method_name] = [
			'namespace' => $namespace,
			'method' => $method_name,
			'is_dynamic_method' => $is_object_method,
		];
	}

	static public function setResponse($api_message, $status = 'success', $code = 1)
	{
		$placeholder = 'api_response';

		$response = PData::getPD($placeholder, []);

		if (!$status) {
			$status = 'error';
			$code = 0;
		}

		if (!empty($response)) {
			if (isset($response['code'])) {
				$tmp_response = [];
				$tmp_response[] = $response;

				$response = $tmp_response;

				unset($tmp_response);
			}

			$response[] = [
				'code' => $code,
				'status' => $status,
				'data' => $api_message,
			];
		} else {
			$response = [
				'code' => $code,
				'status' => $status,
				'data' => $api_message,
			];
		}

		\PData::setPD($placeholder, $response);
	}
}
