<?php
/**
 * Core class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013-2017 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

class PData
{
	const HOMEPAGE_TYPE = 'homepage';
	const CRON_PAGE_TYPE = 'cron';
	const ERROR_PAGE_TYPE = '404';

	#Array with all settings from configuration file
	static private $configs;

	static private $_get = [];
	static private $_post = [];
	static private $_files = [];
	static private $_session = [];
	static private $page_data = [];

	static private $isHomepage = FALSE;

	public function __construct()
	{
		throw new BadMethodCallException('Wrong using. Class PData has only static methods');
	}

	/**
	 * Returns global vars value
	 *
	 * @return object
	 * [
	 * 		_GET => array,
	 * 		_POST => array,
	 * 		_FILES => array,
	 * 		_SESSION => array,
	 * ]
	 */
	static public function global_var()
	{
		return (object) [
			'_GET' => self::$_get,
			'_POST' => self::$_post,
			'_FILES' => self::$_files,
			'_SESSION' => self::$_session,
		];
	}

	/**
	 * Initialization Page data
	 **/
	static public function init()
	{
		if (!empty(self::$configs)) {
			throw new BadMethodCallException('You can\'t initialize PData in second time');
		}

		self::$configs = require_once APP_ROUTE . '_config/_settings.php';

		if (!isset(self::$configs['include_modules'])) {
			self::$configs['include_modules'] = [];
		}

		if (defined('PAGE_TYPE') && PAGE_TYPE == static::CRON_PAGE_TYPE) {// Collect $_GET vars in cron mode
			if (!empty($_SERVER['argv'])) {
				foreach ($_SERVER['argv'] as $arg) {
					if (substr($arg, 0, 2) === '--') {
						$arg = explode('=', substr($arg, 2));

						self::$_get[$arg[0]] = $arg[1];
					}
				}
			}
		} elseif (isset($_GET)) {
			self::$_get = $_GET;
		}

		if (isset($_POST)) {
			self::$_post = $_POST;
		}

		if (isset($_SESSION)) {
			self::$_session = $_SESSION;
		}

		if (isset($_FILES)) {
			self::$_files = $_FILES;
		}

		/** Define modules settings **/
		if (!isset(self::$configs['URL_rules'])) {
			self::$configs['URL_rules'] = array();
		}

		if ($module_settings = glob(APP_ROUTE . '_modules/*/')) {
			foreach ($module_settings AS $settings) {
				if (file_exists($settings . '_settings.php')) {
					$module_name = basename($settings);

					self::$configs['module_settings'][$module_name] = require_once $settings . '_settings.php';

					/* Add module settings to global settings */

					//Add include_apps settings
					if (isset(self::$configs['module_settings'][$module_name]['include_apps'])) {
						self::$configs['include_apps'] += self::$configs['module_settings'][$module_name]['include_apps'];
					}

					//Add include_modules settings
					if (isset(self::$configs['module_settings'][$module_name]['allowed_for_page_types'])) {
						if (!isset(self::$configs['include_modules'][$module_name])) {
							self::$configs['include_modules'][$module_name] = [];
						}

						self::$configs['include_modules'][$module_name] += self::$configs['module_settings'][$module_name]['allowed_for_page_types'];
					}

					//Add URL_rules settings
					if (isset(self::$configs['module_settings'][$module_name]['URL_rules'])) {
						self::$configs['URL_rules'] = array_merge(self::$configs['module_settings'][$module_name]['URL_rules'], self::$configs['URL_rules']);
					}

					//Add init_callbacks settings (Early activities)
					if (isset(self::$configs['module_settings'][$module_name]['init_callbacks'])) {
						foreach (self::$configs['module_settings'][$module_name]['init_callbacks'] as $hook_key => $val) {
							switch ($hook_key) {
								case 'action':
								case 'filter':
									foreach (self::$configs['module_settings'][$module_name]['init_callbacks'][$hook_key] as $sort => $method) {
										self::$configs['init_callbacks'][$hook_key][$sort][] = $method;
									}
								break;

								default:
									if (is_string($hook_key)) {
										self::$configs['init_callbacks'][$hook_key] = $val;
									} elseif (is_int($hook_key)) {
										self::$configs['init_callbacks'][] = $val;
									}
							}
						}

						self::$configs['init_callbacks'] = array_merge(self::$configs['module_settings'][$module_name]['init_callbacks'], self::$configs['init_callbacks']);
					}
				}
			}
		}

		if (!empty(uAPI::$URL_rules)) {
			self::$configs['URL_rules'][uAPI::PAGE_TYPE] = uAPI::$URL_rules;
		}
	}

	/**
	 * Return main configs
	 *
	 * @param string $key
	 * @param string|null $param
	 *
	 * @return mixed
	 **/
	static public function config($key, $param = null)
	{
		if (!is_null($param) && !is_scalar($param)) {
			throw new InvalidArgumentException('$param must be NULL or scalar');
		}

		$result = [];

		if (isset(self::$configs[$key])) {
			if ($param !== NULL) {
				if (self::$configs[$key][$param]) {
					$result = self::$configs[$key][$param];
				}
			} else {
				$result = self::$configs[$key];
			}
		} elseif (!isset(self::$configs)) {
			self::init();

			if (!isset(self::$configs[$key])) {
				return $result;
			} else {
				return self::config($key, $param);
			}
		}

		return $result;
	}

	/**
	 * Rewrites config values
	 *
	 * @param string $key
	 * @param string|array $value
	 *
	 * @return bool
	 */
	static public function setConfigs($key, $value)
	{
		self::$configs[$key] = $value;

		return TRUE;
	}

	/**
	 * Check current page on Homepage, return TRUE, if it is Homepage and return $return if it is not
	 *
	 * @param mixed $return - returns this value if current pagу is not Homepage
	 *
	 * @return mixed
	 */
	static public function isHomepage($return = FALSE)
	{
		### Get cached value ===

		if (self::$isHomepage || defined('PAGE_TYPE')) {
			self::$isHomepage = TRUE;

			if (self::isPageType(self::HOMEPAGE_TYPE)) {
				return TRUE;
			}

			return $return;
		}


		### Define self::$isHomepage value ===
		self::$isHomepage = TRUE;
		// Get HOMEPAGE URL.
		// Use it if you redirect Default HOMEPAGE to custom page and wish to define this custom page as HOMEPAGE.
		// For example: 'http://url.com/en/'
		$homepages = uCode::run_filter('isHomepage', [HOMEPAGE]);

		if (
			CANONICAL_PAGE_URL == HOMEPAGE
			|| in_array(PAGE_URL, $homepages)
			|| (
				($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == FALSE)
				&& in_array(CANONICAL_PAGE_URL, $homepages)
			)
		) {
			$return = TRUE;
		} elseif ($_SERVER['REQUEST_URI'][1] == '?' && in_array(CANONICAL_PAGE_URL, $homepages)) { //Define excepted $_GET elements for HOMEPAGE
			parse_str(substr($_SERVER['REQUEST_URI'], 2), $gets);

			if (!empty(self::config('homepage_settings')['exception_get_variables'])) {
				foreach (self::config('homepage_settings')['exception_get_variables'] as $allowed_get) {
					unset($gets[$allowed_get]);
				}
			}

			if (empty($gets)) {
				$return = TRUE;
			}
		}

		return $return;
	}

	static public function isPageType($page_type)
	{
		return ($page_type == PAGE_TYPE);
	}

	/**
	 * Get or set value of $_GET variable
	 * @param  string/int $_GET_key - key of $_GET array
	 * @param  string  $val - value of self::$_get[$key]
	 * @param  boolean $set - if TRUE, than will do self::$_get[$key] = $val;
	 * @return mixed - return value of property
	 **/
	static public function _GET($_GET_key, $val = '', $set = FALSE)
	{
		if (!is_string($_GET_key) && !is_int($_GET_key)) {
			throw new _InvalidArgumentException('$_GET_key must be integer or string');
		}


		return self::getSetArrayValues('_get', $_GET_key, $val, $set);
	}

	/**
	 * Get or set value of $_POST variable
	 * @param  string/int $_POST_key - key of $_POST array
	 * @param  string  $val - value of self::$_post[$key]
	 * @param  boolean $set - if TRUE, than will do self::$_post[$key] = $val;
	 *
	 * @return string|array|int - return value of property
	 **/
	static public function _POST($_POST_key, $val = '', $set = FALSE)
	{
		if (!is_string($_POST_key) && !is_int($_POST_key)) {
			throw new _InvalidArgumentException('$_POST_key must be integer or string');
		}


		return self::getSetArrayValues('_post', $_POST_key, $val, $set);
	}

	/**
	 * Get or set value of $_FILES variable
	 * @param  string/int $_FILES_key - key of $_FILES array
	 * @param  string  $val - value of self::$_files[$key]
	 * @param  boolean $set - if TRUE, than will do self::$_files[$key] = $val;
	 * @return mixed - return value of property
	 **/
	static public function _FILES($_FILES_key, $val = '', $set = FALSE)
	{
		if (!is_string($_FILES_key) && !is_int($_FILES_key)) {
			throw new _InvalidArgumentException('$_FILES_key must be integer or string');
		}


		return self::getSetArrayValues('_files', $_FILES_key, $val, $set);
	}

	static private function getSetArrayValues($property_name, $array_key, $array_value, $set)
	{
		if (!property_exists('PData', $property_name)) {
			throw new _InvalidArgumentException('Property "' . $property_name . '" is not exists');
		}

		//Prepare _GET element keys

		parse_str($array_key, $output);

		$orders = [];

		do {
			$next_output = [];

			foreach ($output as $item_key => $value) {
				$orders[] = $item_key;
				unset($output[$item_key]);

				if (!empty($value)) {
					$output = $value;
				}
			}
		} while (!empty($output));

		$property_value = self::$$property_name;
		$get_orders = $set_orders = $orders;


		//Set Array value

		if ($set) {
			$set_orders = array_reverse($set_orders); //It needs to start from last child element
			$first_key_element = $set_orders[0];
			$tmp = array();

			if (count($set_orders) > 1) {
				foreach ($set_orders as $value) {
					if ($first_key_element == $value){//Last parent element
						$tmp[$first_key_element] = $array_value;
					} else {
						$tmp = [
							$value => $tmp,
						];
					}
				}
			} elseif (count($set_orders) == 1) {
				$tmp[$set_orders[0]] = $array_value;
			}

			$property_value = _arrays_adding($property_value, $tmp);
		}


		//Get Array value

		$result = NULL;

		foreach ($get_orders as $order_key => $value) {
			if (isset($property_value[$value])) {
				$result = $property_value[$value];

				unset($get_orders[$order_key]);
			} elseif (isset($result[$value])) {
				$result = $result[$value];

				unset($get_orders[$order_key]);
			}
		}

		if (!empty($get_orders)) {
			$result = NULL;
		}

		self::$$property_name = $property_value;

		return $result;
	}

	/**
	 * Get or set value of $_SESSION variable
	 * @param  string/int $key - key of $_SESSION array
	 * @param  string  $val - value of self::$_session[$key]
	 *
	 * @return string|array|int - return value of property
	 **/
	static public function _SESSION($key, $val = '', $set = FALSE)
	{
		if ($set) {
			self::$_session[$key] = $val;
		}

		if (isset(self::$_session[$key])) {
			return self::$_session[$key];
		}

		return NULL;
	}

	/**
	 * Clear all data
	 *
	 * @return void
	 *
	 **/
	static function clear()
	{
		self::$configs = [];
		self::$_get = [];
		self::$_post = [];
		self::$_session = [];
		self::$page_data = [];
	}

	/**
	 * Termination of the script
	 *
	 * @return FALSE
	 *
	 **/
	static public function redirect($url = HOMEPAGE, $header_code = '')
	{
		switch ($header_code) {
			case'404':
				header('HTTP/1.0 404 not found');

				if (!empty(self::config('errors')['404'])) {
					header('Location: ' . self::config('errors')['404']);
				}

				echo '<h1>ERROR 404 - page not found</h1>';
			break;

			case'301':
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $url);

			break;

			default:
				header('Location: ' . $url);
			break;
		}
		exit();
	}

	//TODO: needs to test in correct work with HTTP URLs
	/**
	 * Downloading file through URL
	 *
	 * @param string $source_url
	 * @param string $download_file_name
	 *
	 * @return void
	 **/
	static public function download($source_url, $download_file_name)
	{
		if (!_checkURL($source_url)){
			header ("HTTP/1.0 404 Not Found");
		}

		$fd = @fopen($source_url, "rb");

		if (!$fd){
			header ("HTTP/1.0 403 Forbidden");
			exit;
		}

		header("HTTP/1.1 200 OK");
		header("Content-Disposition: attachment; filename=" . $download_file_name);
		header("Content-Type: application/downloads");

		while(!feof($fd)){
			$content = fread($fd, 4096);
			print($content);
		}
	}

	/**
	 * Set page data in global odject for current page
	 * @param string $page_title - title of current page
	 * @param array $breadcrumb - array with values for building breadcrumb
	 *
	 * @return mixed
	 *
	 **/
	static public function setPageData($data_key, $data_value)
	{
		return self::$page_data[ $data_key ] = $data_value;
	}

	static public function setPD($data_key, $data_value)
	{
		return self::setPageData($data_key, $data_value);
	}

	/**
	 * Save message for user (front-end)
	 *
	 * @param  string|array $message_text - one message or array of message
	 * @param  bool|null $is_positive_message
	 * 		- set `true` to make positive message
	 * 		- set `false` to make error message
	 * 		- set `null` to make warning message
	 *
	 * @return mixed
	 *
	 * @throws _InvalidArgumentException
	 * 			if $message_text is not string or array
	 **/
	static public function setMessage($message_text, $is_positive_message = TRUE)
	{
		$placeholder = 'success_message';

		if (!$is_positive_message) {
			$placeholder = 'error_message';

			if (is_null($is_positive_message)) {
				$placeholder = 'warning_message';
			}
		}

		$messages = self::getPageData($placeholder, []);

		if (is_array($message_text)) {
			$messages += $message_text;
		} elseif (is_string($message_text)) {
			$messages[] = $message_text;
		} else {
			throw new InvalidArgumentException('$message_text must be string or array');
		}

		return self::setPageData($placeholder, $messages);
	}

	/**
	 * @deprecated
	 **/
	static public function message($message)
	{
		return self::setMessage($message, TRUE);
	}

	/**
	 * Make error message
	 * @param string|array|object $message
	 * @param string $type - type of error
	 * @param bool
	 **/
	static public function error($message)
	{
		return self::setMessage($message, FALSE);
	}

	/**
	 * Get page data in global object for current page
	 * @param string $data_key - the key of value as is needed
	 *
	 * @return array | string | int | false - if failed
	 *
	 **/
	static public function getPageData($data_key, $return = FALSE)
	{
		if (isset(self::$page_data[$data_key])) {
			return self::$page_data[$data_key];
		}

		return $return;
	}

	static public function getPD($data_key, $return = FALSE)
	{
		return self::getPageData($data_key, $return);
	}

	/**
	 * Return URL string
	 *
	 * @param string $PAGE_TYPE - page type of URL rule
	 * @param array|mixed $URL_data - can has array or scalar value
	 *
	 * @return string
	 *
	 * @throws InvalidArgumentException
	 *          if $PAGE_TYPE is not a string or is empty
	 *          if $URL_data is not a scalar or array
	 *          if in $URL_data does not exist required elements of URL
	 **/
	static public function getURL($PAGE_TYPE, $URL_data = false)
	{
		if (!is_string($PAGE_TYPE)) {
			throw new InvalidArgumentException('$PAGE_TYPE must be not empty string');
		}

		if (!is_scalar($URL_data) && !is_array($URL_data)) {
			throw new InvalidArgumentException('$URL_data must be scalar or array');
		}

		$result_url = '';
		$gets = $URL_data;

		if ($PAGE_TYPE != PData::HOMEPAGE_TYPE) {

			if (!isset(self::$configs['URL_rules'][$PAGE_TYPE])) {
				return '404';
			}

			//Set page_type _GET value for URL building process
			if (!is_array($URL_data)) {
				$URL_data = [$PAGE_TYPE => $URL_data];
			}

			$url = self::$configs['URL_rules'][$PAGE_TYPE];

			if (!is_array($url)) {
				$url = [$url];
			}

			foreach ($url as $url_rule) {
				$url_rule = str_replace('?', '', $url_rule);
				$gets = [];
				$tmp_url = $url_rule = uCode::run_multi_filter('_getURL', $url_rule, $URL_data);

				foreach ($URL_data as $get_name => $get_value) {
					$url_rule = str_replace(["{{$get_name}}", "[{$get_name}]"], $get_value, $url_rule);
					if ($url_rule == $tmp_url) {
						$gets[$get_name] = $get_value;
					} else {
						$tmp_url = $url_rule;
					}
				}

				if (!strstr($url_rule, '{') && !strstr($url_rule, '[')) {
					$result_url = $url_rule;
					break;
				}
			}

			if (!$result_url) {
				throw new InvalidArgumentException('Missed one or more URL rule elements of \'' . $PAGE_TYPE . '\'. The rule(s) of this PAGE_TYPE: ' . implode(', ', $url));
			}
		}

		$url = HOMEPAGE . $result_url;

		if (!empty($gets)) {
			$url .= '?' . http_build_query($gets);
		}

		return uCode::run_multi_filter('_getURL_after', $url, [
			'PAGE_TYPE' => $PAGE_TYPE,
			'URL_data' => $URL_data,
		]);
	}

	static public function validation()
	{
		return new sub_classes\validation();
	}

	static public function cURL($url, $parameters = [])
	{
		return new \sub_classes\cURL($url, $parameters);
	}
}