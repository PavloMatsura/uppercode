<?php
/**
 * SQL Migrations
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2017 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 *
 * @example
 *      uDBMigration::table('_clients')
 *			->setColumn('cl_id', [
 *				'type' => 'int(11)',
 *				'auto_increment' => TRUE,
 *				'key' => [
 *					'PRI' => TRUE,
 *				]
 *			])
 *          ->setColumn('fullname', [
 *              'type' => 'varchar(255) NOT NULL',
 *              'default' => NULL
 *          ])
 *          ->renameColumn('fullname', 'last_name')
 *          ->deleteColumn('last_name')
 *          ->execute();
 */

class uDBMigration
{
	private $table = '';
	private $query_result = [];
	private $columns_list = [];
	private $modify_columns = [];
	private $delete_columns = [];
	private $add_columns = [];
	private $rename_columns = [];
	private $query = [];
	private $connection_name = FALSE;

	/**
	 * @return uDBMigration
	 **/
	static public function table($table, $connection_name = FALSE)
	{
		if (empty($table)) {
			throw new InvalidArgumentException('$table name should be not empty string.');
		}

		$schema = new uDBMigration();

		$schema->connection_name = $connection_name;

		if (!$connection_name) {
			$schema->connection_name = uDB::getLastConnectionName();
		}

		if (empty(PData::config('db_settings')[$schema->connection_name]['db_name'])) {
			throw new InvalidArgumentException('Invalid DB connection settings');
		}

		
		$schema->table = $table;
		$db_name= PData::config('db_settings')[$schema->connection_name]['db_name'];

		$query = "	SELECT *
					FROM INFORMATION_SCHEMA.COLUMNS
					WHERE table_name = :table 
						AND table_schema = :db_name";
		
		$schema->query_result = uDB::connect($schema->connection_name)
									->query($query)
									->binds([
										':table' => $schema->table,
										':db_name' => $db_name,
									])
									->execute()
									->fetch_array();

		$schema->columns_list = array_column($schema->query_result, 'COLUMN_NAME');

		return $schema;
	}

	/**
	 * Modify or Add column, if not exists
	 *
	 * @param string $column_name
	 * @param string | array $properties
	 *
	 * @example
	 *      $this->setColumn('column_name', [
	 *              'type' => 'int(11) DEFAULT "name"',
	 *				'PRI' => TRUE,
	 *              'UNI' => FALSE,
	 *              'AI' => FALSE,
	 *      ]);
	 *
	 * @return uDBMigration
	 *
	 **/
	public function setColumn($column_name, $properties)
	{
		if (is_string($properties)) {
			$properties = [
				'type' => $properties,
			];
		} elseif (!is_array($properties)) {
			throw new _InvalidArgumentException('$properties should be not empty string or array.');
		}

		if (!is_string($column_name) || empty($column_name)) {
			throw new _InvalidArgumentException('$column_name should be not empty string.');
		}

		$column_key = array_search($column_name, $this->columns_list);

		if ($column_key) {
			$column = $this->query_result[$column_key];

			foreach ($properties as $key => $property) {
				if (!self::compareProperty($key, $property, $column)) {
					unset($properties[$key]);
				}
			}

			if (!empty($properties) && !in_array($column_name, $this->columns_list)) {
				$this->modify_columns[$column_name] = $properties;
				$this->prepareAlterTable('MODIFY', $column_name, $properties);
			}
		} elseif (!in_array($column_name, $this->columns_list)){
			$this->prepareAlterTable('ADD', $column_name, $properties);
		}

		return $this;
	}

	/**
	 * Drop column from table
	 *
	 * @param string $column_name - name of column, which needs to delete
	 *
	 * @example
	 *      $this->deleteColumn('column_name_to_delete');
	 *
	 * @return uDBMigration
	 *
	 **/
	public function deleteColumn($column_name)
	{
		if (!is_string($column_name) || empty($column_name)) {
			throw new _InvalidArgumentException('$column_name should be not empty string');
		}

		if (in_array($column_name, $this->columns_list)) {
			$this->prepareAlterTable('DROP', $column_name);
		}

		return $this;
	}

	/**
	 * Rename column
	 *
	 * @param string $column_name - name of column, which needs to delete
	 *
	 * @example
	 *      $this->renameColumn('old_column_name', 'new_column_name');
	 *
	 * @return uDBMigration
	 *
	 **/
	public function renameColumn($old_column_name, $new_column_name)
	{
		if (!is_string($old_column_name) || empty($old_column_name)) {
			throw new _InvalidArgumentException('$old_column_name should be not empty string.');
		}

		if (!is_string($new_column_name) || empty($new_column_name)) {
			throw new _InvalidArgumentException('$new_column_name should be not empty string.');
		}

		if (in_array($old_column_name, $this->columns_list)) {
			$this->prepareAlterTable('RENAME', $old_column_name, $new_column_name);
		}

		return $this;
	}

	/**
	 * Prepare ALTER TABLE queries
	 * @param string | array $value
	 *
	 * @example
	 * # RENAME
	 *      $this->prepareAlterTable('RENAME', 'old_column_name', 'new_column_name');
	 *
	 * # DROP
	 *      $this->prepareAlterTable('DROP', 'column_name');
	 *
	 * # ADD | MODIFY
	 *      $this->prepareAlterTable('ADD', 'column_name', [
	 *              'type' => 'int(11) NOT NULL',
	 *				'key' => [
	 *                  'PRI' => TRUE,
	 *                  'UNI' => FALSE,
	 *                  'AI' => TRUE,
	 *              ],
	 *      ]);
	 **/
	private function prepareAlterTable($action, $column_name, $value = '')
	{
		switch ($action) {
			case 'RENAME':
				//FIXME: It does not work
				break;
				if (empty($value)) {
					throw new _InvalidArgumentException('New column name should be not empty string.');
				}

				$this->query[] = "CHANGE COLUMN '{$column_name}'
{$value};" ;
			break;
			case 'DROP':
				$this->query[] = " DROP COLUMN `{$column_name}`;";
			break;
			case 'ADD':
			case 'MODIFY':
				$query_keys = $query = [];

				//Set column data : int(11) NOT NULL DEFAULT ""
				if (isset($value['type'])) {
					$query[] = $value['type'];
				}

				//Set key: PRIMARY KEY and UNIQUE KEY
				if (isset($value['key']) || isset($value['PRI']) || isset($value['UNI']) || isset($value['AI'])) {
					$keys = [];

					if (!empty($value['key']) && is_array($value['key'])) {
						$keys = $value['key'];
					}

					if (isset($value['PRI'])) {
						$keys['PRI'] = (bool)$value['PRI'];
					}

					if (isset($value['UNI'])) {
						$keys['UNI'] = (bool)$value['UNI'];
					}

					if (isset($value['AI'])) {
						$keys['AI'] = (bool)$value['AI'];

						if (!isset($keys['UNI']) || !$keys['UNI'] ) {
							$keys['UNI']  = TRUE;
						}
					}

					foreach ($keys as $key => $val) {
						switch ($key) {
							case 'PRI':
								if ($val) {
									$query_keys['PRI'][] = ' DROP PRIMARY KEY; ';
									$query_keys['PRI'][] = " ADD PRIMARY KEY (`{$column_name}`);";
								}
							break;
							case 'UNI':
								if ($val) {
									$query_keys['UNI'] = " ADD UNIQUE KEY `{$column_name}` (`{$column_name}`);";
								} else {
									$query_keys['UNI'] = " DROP INDEX `{$column_name}`;";
								}
							break;
							case 'AI':
								if (!empty($value['type'])) {
									if (strpos($value['type'], 'int') === FALSE) {
										throw new _InvalidArgumentException('Type of AUTO_INCREMENT column "' . $column_name . '" should be integer.');
									} else {
										//										$query[] = " {$column_name} {$value['type']} ";
										$query_keys['AI'] = " MODIFY `{$column_name}` {$value['type']} AUTO_INCREMENT; ";
									}

								} else {
									$query_keys['AI'] = " MODIFY `{$column_name}` int(11) NOT NULL AUTO_INCREMENT; ";
								}
							break;
						}
					}
				}

				# Preparing query
				if (!empty($query)) {
					$value = implode(' ', $query);

					if (!empty($this->query_result)) {
						$this->query[] = " {$action} `{$column_name}` {$value};";
					} else {
						$this->add_columns[] = " `{$column_name}` {$value} ";
					}
				}

				if (!empty($query_keys['PRI'])) {
					foreach ($query_keys['PRI'] as $val) {
						$this->query[] = $val;
					}
				}

				if (isset($query_keys['UNI'])) {
					$this->query[] = $query_keys['UNI'];
				}

				if (isset($query_keys['AI'])) {
					$this->query[] = $query_keys['AI'];
				}
			break;
		}

		return TRUE;
	}

	public function execute()
	{
		if (empty($this->query)) {
			return FALSE;
		}

		$query = '';

		if (!empty($this->add_columns)) {
			$query = " 	CREATE TABLE IF NOT EXISTS `{$this->table}`
						(\n" . implode(",\n", 
							$this->add_columns) . "
						)                 
						ENGINE=InnoDB DEFAULT CHARSET=utf8;" ;
		}

		if (!empty($this->query)) {
			$query .= "
					ALTER TABLE `{$this->table}`\n" . 
						implode("

					ALTER TABLE `{$this->table}`\n", 
						$this->query);
		}
		// _dump($query,1,1);
		return (bool)uDB::connect($this->connection_name)
						->query($query)
						->execute()
						->last_insert_id();
	}

	private static function compareProperty($new_prop_name, $new_prop_val, $column_properties)
	{
		switch ($new_prop_name) {
			case 'type':
				if ($new_prop_val != $column_properties['COLUMN_TYPE']) {
					return TRUE;
				}
			break;
			case 'key':
				return TRUE;
			break;
			case 'auto_increment':
				if ($new_prop_val != (bool)$column_properties['EXTRA']) {
					return TRUE;
				}
			break;
			case 'default':
				if ($new_prop_val != $column_properties['COLUMN_DEFAULT']) {
					return TRUE;
				}
			break;
		}
	}
}