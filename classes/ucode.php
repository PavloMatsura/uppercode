<?php
/**
 * Core class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

class uCode
{
	static private $hook = array(
		'action' => array(),
		'filter' => array(),
	);

	#Timer execution speed of a script
	static private $countTime = [];
	static private $initFlag = FALSE;
	static public $hooks_init_log = '';

	public function __construct()
	{
		throw new \BadMethodCallException('Wrong using. Class uCode has only static methods');
	}

	/**
	 * Code initialization of framework
	 * - Settings defining
	 * - Early callbacks
	 * - URL parsing and $_GET elements defining
	 * - PAGE_TYPE defining
	 **/
	static public function init()
	{
		if (self::$initFlag) {
			throw new InvalidArgumentException('You can\'t initialize uCode in second time');
		}

		#Set session
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}

		PData::init();

		//Callback functions which will calling after config defined
		if (PData::config('init_callbacks')) {
			foreach (PData::config('init_callbacks') as $key => $value) {
				switch ($key) {
					case 'action':
					case 'filter': continue;
				}

				if (is_array($value)) {
					if (isset($value['class_name']) && isset($value['method'])) {
						$class_name = $value['class_name'];
						$method_name = $value['method'];

						unset($value['class_name'], $value['method']);

						if (method_exists($class_name, $method_name)) {
							call_user_func_array([$class_name, $method_name], [$value]);
						}
					} elseif (is_string($key)) {
						if (function_exists($key)) {
							$key($value);
						}
					}
				}
			}

			//Early actions initialisation
			if (!empty(PData::config('init_callbacks')['action'])) {
				foreach (PData::config('init_callbacks')['action'] as $action_key => $value) {
					ksort($value);

					foreach ($value as $sorting => $functions) {
						foreach ($functions as $function_name) {
							self::add_action($action_key, $function_name, $sorting);
						}
					}
				}
			}

			//Early filters initialisation
			if (!empty(PData::config('init_callbacks')['filter'])) {
				foreach (PData::config('init_callbacks')['filter'] as $action_key => $value) {
					ksort($value);

					foreach ($value as $sorting => $functions) {
						foreach ($functions as $function_name) {
							self::add_filter($action_key, $function_name, $sorting);
						}
					}
				}
			}
		}

		self::run_action('defined_config');

		if (!defined('PAGE_TYPE')) {
			self::run_action('before_page_type_defining');

			$defined_page_type = FALSE;

			//Parse URL and define PAGE_TYPE and $_GET elements
			foreach (PData::config('URL_rules') as $page_type => $rule) {
				foreach ((array)$rule as $value) {
					if (self::parseURLRules($page_type, $value)) {
						$defined_page_type = TRUE;

						break(2);
					}
				}
			}

			if (!$defined_page_type) {
				if (PData::isHomepage()) {
					define('PAGE_TYPE', PData::HOMEPAGE_TYPE);
				} else {
					define('PAGE_TYPE', PData::ERROR_PAGE_TYPE);
				}
			}
		}

		self::$initFlag = TRUE;

		self::run_action('defined_PAGE_TYPE');
	}

	static private function parseURLRules($page_type, $rule)
	{
		if (substr($rule, -1) == '?') {
			$rule .= '/?';
		}

		//Pripare pattern for URL parsing
		$pattern = str_replace('/', '\/', $rule);
		$pattern = preg_replace('/(\{[a-zA-Z0-9\.\-_]+\})/Uis', '([a-zA-Z0-9\.\-_]+)', $pattern);

		$pattern = preg_replace('/(\[[a-zA-Z0-9\.\-_]+\])/Uis', '([a-zA-Z0-9\.\/\-_]+)', $pattern);
		$pattern = '/^(https|http):\/\/([a-zA-Z0-9\.\-_]+)\/' . $pattern . '$/';

		//URL parsing
		preg_match_all($pattern, CANONICAL_PAGE_URL, $gets);

		//Defining $_GET array and page_type
		if (!empty($gets[0])) {
			preg_match_all('/(\{|\[)([a-zA-Z0-9\.\-_]+)(\}|\])/', $rule, $get_values);

			unset($gets[0], $gets[1], $gets[2]);

			$i = 0;

			foreach ($gets as $get) {
				PData::_GET($get_values[2][$i], $get[0], true);
				$i++;
			}

			define('PAGE_TYPE', $page_type);

			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Add hook of type 'action'
	 * @param string $key - key of action
	 * @param string|array $function - function, what will initialized
	 * @param string $sorting - priority functions initialisation (default:10)
	 *
	 * * @example
	 * 	// Add method to action
	 * 	uCode::add_action('adminPanel_sidebar_menu', ['\mod\admin' => 'addMenuItem']);
	 *
	 * 	//Add function to action
	 * 	uCode::add_action('adminPanel_sidebar_menu', 'addMenuItem');
	 *
	 * @return boolean|string|array
	 */
	static public function add_action($key, $function, $sorting = 10)
	{
		return uCode::add_hook($key, $function, 'action', $sorting);
	}

	/**
	 * Add hook of type 'filter'
	 * @param string $key - key of action
	 * @param string|array $function - function, what will initialized
	 * @param string $sorting - priority functions initialisation (default:10)
	 *
	 * @example
	 * 	// Add method to filter
	 * 	uCode::add_filter('adminPanel_sidebar_menu', ['\mod\admin' => 'addMenuItem']);
	 *
	 * 	//Add function to filter
	 * 	uCode::add_filter('adminPanel_sidebar_menu', 'addMenuItem');
	 *
	 * @return bool|string|array
	 */
	static public function add_filter($key, $function, $sorting = 10)
	{
		return uCode::add_hook($key, $function, 'filter', $sorting);
	}

	/**
	 * Add hook
	 * @param string $key
	 * @param string $function
	 * @param string $hook_type - (action|filter)
	 * @param string|int|float $sorting
	 *
	 * @return bool
	 **/
	static private function add_hook($key, $function, $hook_type, $sorting)
	{
		switch($hook_type){
			case'action':case'filter':
				self::$hook[$hook_type][$key][(string)$sorting][] = $function;

				return TRUE;
			break;

			default:
				return FALSE;
		}
	}

	/**
	 * Initializing all functions and methods, which was added to current action
	 *
	 * @param string $key
	 * @param string $accepted_vars
	 *
	 * @return mixed
	 */
	static public function run_action($key, $accepted_vars = '')
	{
		return self::run_hook($key, '', $accepted_vars, 'action');
	}

	/**
	 * The same as run_action(), but this running will not clean collected function and method data.
	 * So it can be run few times
	 *
	 * @param string $key
	 * @param string $accepted_vars
	 *
	 * @return mixed
	 */
	static public function run_multi_action($key, $accepted_vars = '')
	{
		return self::run_hook($key, '', $accepted_vars, 'action', TRUE);
	}

	/**
	 * Initializing all functions and methods, which was added to current filter
	 *
	 * @param string $key
	 * @param string $result
	 * @param string $accepted_vars
	 *
	 * @return mixed
	 */
	static public function run_filter($key, $result = '', $accepted_vars = '')
	{
		return self::run_hook($key, $result, $accepted_vars, 'filter');
	}

	/**
	 * The same as run_filter(), but this running will not clean collected function and method data.
	 * So it can be run few times
	 *
	 * @param string $key
	 * @param string $result
	 * @param string $accepted_vars
	 *
	 * @return mixed
	 */
	static public function run_multi_filter($key, $result = '', $accepted_vars = '')
	{
		return self::run_hook($key, $result, $accepted_vars, 'filter', TRUE);
	}

	/**
	 * Run functions and methods for current hook
	 *
	 * @param string $key
	 * @param string $result
	 * @param string $accepted_vars
	 * @param bool $hook_type
	 * @param bool $multi_hook
	 *
	 * @return mixed
	 */
	static private function run_hook($key, $result = '', $accepted_vars = '', $hook_type = FALSE, $multi_hook = FALSE)
	{
		switch ($hook_type) {
			case'action':case'filter':
				self::$hooks_init_log .= "{$hook_type}: {$key}\n";

				if (isset(self::$hook[$hook_type][$key])) {
					$functions = self::$hook[$hook_type][$key];

					if (!$multi_hook) {
						unset(self::$hook[$hook_type][$key]);
					}
				} elseif ($hook_type == 'filter') {
					return $result;
				} else {
					return FALSE;
				}
			break;

			default:
				return FALSE;
		}

		if (!empty($functions)) {
			ksort($functions);

			foreach ($functions as $sorting => $function) {
				if (is_array($function)) {
					foreach ($function as $key => $fname) {
						switch($hook_type){
							case'action':
								if (is_array($fname)) { //OOP calling
									if (method_exists($fname[0], $fname[1])) {
										$teml_result = call_user_func_array([$fname[0], $fname[1]], [$accepted_vars]);

										if (is_scalar($teml_result) && is_scalar($result)) {
											$result .= $teml_result;
										} else {
											$result = $teml_result;
										}
									}
								} elseif (function_exists($fname)) { //Function calling
									$teml_result = $fname($accepted_vars);

									if (is_scalar($teml_result) && is_scalar($result)) {
										$result .= $teml_result;
									} else {
										$result = $teml_result;
									}
								}
							break;

							case'filter':
								if (is_array($fname)) { //OOP calling
									if (!isset($fname[0])) {
										_dump($fname,1,1);
									}
									if (method_exists($fname[0], $fname[1])) {
										$result = call_user_func_array([$fname[0], $fname[1]], [$result, $accepted_vars]);
									}
								} elseif (function_exists($fname)) { //Function calling
									$result = $fname($result, $accepted_vars);
								}
							break;
						}
					}
				}
			}
		}

		return $result;
	}

	/**
	 * Termination of the script
	 *
	 * @return void
	 *
	 **/
	static function clear()
	{
		self::$hook = array(
			'action' => array(),
			'filter' => array(),
		);

		#Timer execution speed of a script
		self::$countTime = [];

		self::$initFlag = FALSE;
	}

	/**
     * @deprecated
	 * Save data in log
	 *
	 * @param mixed $message
	 * @param string $log_mark
	 *
	 * @return bool
	 **/
	static public function saveLog($message, $log_mark = '')
	{
        uBugtracker::init()->deprecated();

		if (empty($log_mark)) {
			$log_mark = date('Y-m-d') . '_main';
		}

		$file_route = CORE_ROUTE . '_logs/' . $log_mark . '_data.log';
		$file_content = '';

		if (file_exists($file_route)) {
			$file_content = file_get_contents($file_route);
		}

		$message =
			"=================================== " . date('Y-m-d H:i:s') . " ===================================\n\n" .
			print_r($message, 1) . "\n" .
			"===========================================================================================\n\n\n\n" .
			$file_content;


		$fp = fopen($file_route, 'w');
		$result = fwrite($fp, $message);

		fclose($fp);

		return (is_bool($result) && !$result) ? $result : TRUE;
	}

	/**
	 * Save cache
	 * @param string $string - string, what will be write in file
	 * @param string $file_name - temp file name
	 * @example
	 * 		uCode::saveCache('Hello World!','temp/'.date('Y-m-d_H-i-s').'.htm');
	 *
	 * @return bool/string - in successful case return count of wrote bytes else return FALSE
	 *
	 **/
	static public function saveCache($string, $file_name = FALSE, $extension = '.cache')
	{
		if (!$file_name) {
			$file_name = date('Y-m-d_H-i-s') . $extension;
		}

		$file_name = CORE_ROUTE . '_cache/' . $file_name;

		if (!file_exists(dirname($file_name))) {
			mkdir(dirname($file_name), 0777);
		}

		$fp = fopen($file_name, 'w');
		$result = fwrite($fp, $string);
		fclose($fp);

		return $result;
	}

	/**
	 * ToDo: method was not tested
	 * Make backup of folder
	 *
	 * @param string|false $file_name - name of backup file
	 * @param string|false $form_folder_route - absolute route to folder, which needs to backup
	 * @param string|false $to_folder_route - absolute route to folder, in which will be saved backup
	 *
	 * @return void
	 */
	static public function saveBackup($file_name = FALSE, $form_folder_route = FALSE, $to_folder_route = FALSE)
	{
		if (!$to_folder_route) {
			$to_folder_route = CORE_ROUTE . 'backups/';
		}

		if (substr($to_folder_route, -1) != '/') {
			$to_folder_route .= '/';
		}

		if (!$file_name) {
			$file_name = 'backup_' . date('d.m.Y_H:i') . '.zip';
		}

		if (substr($file_name, -4) != '.zip') {
			$file_name .= '.zip';
		}

		if (!$form_folder_route) {
			$form_folder_route = BASIC_ROUTE;
		}

		$archive_route = $to_folder_route . $file_name;
		$files_to_arch = array();

		for($d = @opendir($form_folder_route); $file = @readdir($d);){
			if($file!='.' && $file!='..'){
				$files_to_arch[]= $file;
			}
		}

		chdir($to_folder_route);

		$archive = new PclZip($archive_route);

		$archive->create(implode(',', $files_to_arch));
	}

	/**
	 * Remove folder and all child files
	 **/
	static public function unDir($dir,$delete_dir=TRUE)
	{
		if ($objs = glob($dir."*")) {
			foreach ($objs as $obj) {
				is_dir($obj) ? self::unDir($obj . '/') : unlink($obj);
			}
		}

		if(is_dir($dir) && $delete_dir){
			rmdir($dir);
		}
	}
}