<?php
/**
 * Core class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

/**
 * Class uController
 *
 * @property string $module_name - module folder name
 * @property string $module_route - Module absolute server rout. Ending with `/`
 */
abstract class uController
{
	#region Constants

	#region Action list

	const ACT_DEFINED_MODULES = 'defined_modules';
	const ACT_INIT_PAGE = 'init_page';

	#endregion

	#endregion

	/**
	 * @var static[]
	 */
	static private $module_objects;

	static private $allowed_module_actions = [
		self::ACT_DEFINED_MODULES => true,
		self::ACT_INIT_PAGE => true,
	];

	/** @var string */
	private $module_name;
	/** @var string */
	private $module_route;

	#region Abstract methods

	/**
	 * This is the method which called to define module
	 */
	abstract protected function init_module();

	/**
	 * Use it for action `defined_modules` - in this moment all modules is defined already
	 */
	abstract protected function defined_modules();

	/**
	 * Use it for action `init_page`
	 */
	abstract protected function init_page();

	#endregion

	#region Magic methods

	protected function __construct() {}

	public function __get($property_name)
	{
		switch ($property_name) {
			case 'module_name':
			case 'module_route':
				return $this->$property_name;

			default:
				throw new InvalidArgumentException("Property {$property_name} is not exist");
		}
	}

	public function __isset($property_name)
	{
		switch ($property_name) {
			case 'module_name':
			case 'module_route':
				return empty($this->$property_name);

			default:
				throw new InvalidArgumentException("Property {$property_name} is not exist");
		}
	}

	#endregion

	#region Static methods

	/**
	 * Returns object instance of module
	 *
	 * @param string|null $module_name
	 *
	 * @return static
	 */
	static public function builder($module_name = NULL)
	{
		$class_name = get_called_class();
		if (!isset(self::$module_objects[$class_name]) || !(self::$module_objects[$class_name] instanceof static)) {
			if (is_string($module_name)) {
				$object = new static();
				$object->module_name = $module_name;
				$object->module_route = APP_ROUTE . "_modules/{$module_name}/";

				$object->init_module();

				self::$module_objects[$class_name] = $object;
			} elseif ($module_name === NULL) {
				//Module cannot be called before this initialization
				throw new InvalidArgumentException("Undefined object instance of `{$class_name}`");
			}
		}

		return self::$module_objects[$class_name];
	}

	/**
	 * Calls action methods of all defined modules
	 *
	 * @param $action_name
	 * @return mixed
	 *
	 * @throws InvalidArgumentException
	 */
	static public function runModuleActions($action_name)
	{
		if (empty(self::$module_objects)) {
			throw new InvalidArgumentException('Modules was not initialized');
		}

		$result = '';

		if (isset(self::$allowed_module_actions[$action_name])) {
			foreach (self::$module_objects as $moduleObject) {
				$tmp_result = $moduleObject->$action_name();

				if (is_scalar($tmp_result) && is_scalar($result)) {
					$result .= $tmp_result;
				} else {
					$result = $tmp_result;
				}
			}
		} else {
			throw new InvalidArgumentException("Action `{$action_name}` is not allowed");
		}

		return $result;
	}

	#endregion

	#region Getters

	/**
	 * @return static[]
	 */
	static protected function getModuleObjects()
	{
		if (empty(self::$module_objects)) {
			throw new \InvalidArgumentException('Modules was not initialized');
		}

		return self::$module_objects;
	}

	/**
	 * Include needed models in module router
	 *
	 * @param string $for_PAGE_TYPE - PAGE_TYPE for which needs to include models
	 * @param string|array $models - list of model file routes
	 *
	 * @example
	 * 		$this->getModel('my_model_1.php');
	 * 		$this->getModel([
	 * 			'my_model_1.php',
	 * 			'my_model_2.php',
	 * 		]);
	 *
	 * @return void
	 *
	 * @throws InvalidArgumentException
	 *          if model file didn't exists
	 *          if Model file name is invalid
	 **/
	public function getModel($models)
	{
		if (is_string($models)) {
			$model = $models;

			if (substr($model, -4) != '.php') {
				$model .= '.php';
			}

			$file_route = $this->module_route . '_models/' . $model;

			if (!file_exists($file_route)) {
				throw new InvalidArgumentException('Model file does not exists - "' . $file_route . '"');
			}

			require_once $file_route;
		} elseif (is_array($models)) {
			foreach ($models as $model) {
				if (substr($model, -4) != '.php') {
					$model .= '.php';
				}

				$file_route = $this->module_route . '_models/' . $model;

				if (!file_exists($file_route)) {
					throw new InvalidArgumentException('Model file does not exists - "' . $file_route . '"');
				}

				require_once $file_route;
			}
		} else {
			throw new InvalidArgumentException('Invalid Model file name. Model data: ' . print_r($models, true) . '');
		}
	}

	/**
	 * Connect needed view in module class
	 *
	 * @param string $view_file_name - view file name from folder `_views`
	 * @param array $data - list of vars, which uses in view
	 *
	 * @example
	 *      $this->getView('my_view', ['data_var_name' => 'data_var_value']);
	 *
	 * @return string
	 *
	 * @throws InvalidArgumentException
	 *          if $view_file_name empty or not string
	 **/
	public function getView($view_file_name, $data = [])
	{
		if (!is_string($view_file_name) || empty($view_file_name)) {
			throw new InvalidArgumentException('$view_file_name must be not empty string');
		}

		return uTPL::get($view_file_name, self::builder()->module_name, $data);
	}

	#endregion
}
