<?php
/**
 * Core class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 **/

/**
 * This class use PDO and make work with it more comfortable
 *
 * # HOW TO USE
 *
 * If needs to connect to custom DB, use method connect('connection_name').
 * Without connect('connection_name'), it will connect to uDB::DEFAULT_CONNECTION_NAME .
 * It works for all query types (query, update, insert, replace, delete, last_insert_id)
 *
 * SELECT queries
 * ==============
 *
 * $rules = [
 * 		1, 2, 3, 4, 5,
 * ];
 * $query = '	SELECT id, name
 * 				FROM users
 * 				WHERE id = :id
 * 					AND rule IN (:rule)
 * 				LIMIT 10';
 *
 * //Connecting to default DB
 * $users = uDB::query($query)
 * 				->binds([
 * 					':id' => $id,
 * 					':rule' => $rules,
 * 	  			])
 * 	  			->execute()
 * 	  			->fetch_array();
 *
 * //Connecting to custom DB
 * $old_users = uDB::connect('archive')
 * 					->query($query)
 * 					->binds([
 * 						':id' => $id,
 * 						':rule' => $rules,
 * 	  				])
 * 	  				->execute()
 * 	  				->fetch_array();
 *
 *
 * UPDATE queries
 * ==============
 *
 * //Connecting to default DB
 * uDB::update('users')
 * 		->values([
 * 			'rule' => 6,
 * 		])
 * 		->where('rule IN (:rule)')
 * 		->binds([
 * 			':rule' => $rules,
 * 		])
 * 		->execute();
 *
 *
 * INSERT queries
 * ==============
 *
 * //Connecting to default DB
 * uDB::insert('users')
 * 		->values([
 * 			'rule' => 6,
 * 		])
 * 		->execute()
 * 		->last_insert_id();
 *
 * uDB::last_insert_id();
 *
 * //Get last ID from custom DB
 * uDB::last_insert_id('archive');
 *
 * //Insert new and Update duplicated row
 * uDB::insert('users', uDB::NO_DUPLICATION)
 * 		->values([
 * 			'rule' => 6,
 * 		])
 * 		->execute();
 *
 *
 * REPLACE queries
 * ===============
 *
 * //Connecting to default DB
 * uDB::replace('users')
 * 		->values([
 * 			'rule' => 6,
 * 		])
 * 		->execute();
 *
 *
 * DELETE queries
 * ==============
 *
 * //Connecting to default DB
 * uDB::delete('users')
 * 		->where('rule IN (:rule)')
 * 		->binds([
 * 			':rule' => $rules,
 * 		])
 * 		->execute();
 *
 *
 * CLEAR / destroy connections
 * ===========================
 *
 * //Destroy all connections
 * uDB::clear();
 *
 * //Destroy one connection, for example, with connection name 'archive'
 * uDB::clear('archive');
 *
 * uDB::uDB::connect('archive')
 * 		->delete('users')
 * 		->where('rule IN (:rule)')
 * 		->binds([
 * 			':rule' => $rules,
 * 		])
 * 		->execute()
 * 		->clear();
 *
 *
 **/
class uDB
{
	const DEFAULT_CONNECTION_NAME = 'default';
	const NO_DUPLICATION = 'update on duplicate key';

	## SQL types for exporting DB table
	const TYPE_INSERT = 'sql_type_insert';
	const TYPE_INSERT_IGNORE = 'sql_type_insert_ignore';
	const TYPE_UPDATE = 'sql_type_update';
	const TYPE_REPLACE = 'sql_type_replace';

	private static $current_connection_name = FALSE;



	/************  Static methods  *********************/

	/**
	 * Returning Name of last connection
	 *
	 * @return string
	 **/
	public static function getLastConnectionName()
	{
		if (!self::$current_connection_name) {
			self::$current_connection_name = self::DEFAULT_CONNECTION_NAME;
		}

		return self::$current_connection_name;
	}


	/**
	 * Make connection to Data Base
	 * @param string $connection_name - connection name
	 *
	 * @return uDB\DBQueries
	 **/
	public static function connect($connection_name = FALSE)
	{
		if (!$connection_name) {
			$connection_name = self::DEFAULT_CONNECTION_NAME;
		}

		self::$current_connection_name = $connection_name;

		return \uDB\DBQueries::init(self::$current_connection_name);
	}

	/**
	 * Begin MySQL Transaction
	 *
	 * @return \uDB\DBQueries
	 */
	static public function begin()
	{
		return self::connect()
			->begin();
	}

	/**
	 * Commit MySQL Transaction
	 *
	 * @return \uDB\DBQueries
	 */
	static public function commit()
	{
		return self::connect()
			->commit();
	}

	/**
	 * Rollback MySQL Transaction
	 *
	 * @return \uDB\DBQueries
	 */
	static public function rollback()
	{
		return self::connect()
			->rollback();
	}

	/**
	 * Set DB query
	 *
	 * @param string $query
	 *
	 * @return uDB\DBQueries
	 *
	 * @throws _InvalidArgumentException
	 *          if DB connection is not exists
	 **/
	public static function query($query)
	{
		if (!is_string($query) || empty($query)) {
			throw new _InvalidArgumentException('$query must be not empty string');
		}

		return self::connect()
					->query($query);
	}

	/**
	 * Make Update Query
	 *
	 * @param string $table - table name or JOINed tables, which has the same column names
	 *
	 * @return uDB\DBQueries
	 *
	 * @throws _InvalidArgumentException
	 *			if $table is empty or not string
	 **/
	public static function update($table)
	{
		if (!is_string($table) || empty($table)) {
			throw new _InvalidArgumentException('$table must be not empty string');
		}

		return self::connect()
					->update($table);
	}

	/**
	 * Make Insert Query
	 *
	 * @param string $table - table name or JOINed tables, which has the same column names
	 * @param mixed $no_duplication - set uDB::NO_DUPLICATION , if needs to update available rows (works through 'ON DUPLICATE KEY')
	 *
	 * @return uDB\DBQueries
	 *
	 * @throws _InvalidArgumentException
	 *			if $table is empty or not string
	 **/
	public static function insert($table, $no_duplication = FALSE)
	{
		if (!is_string($table) || empty($table)) {
			throw new _InvalidArgumentException('$table must be not empty string');
		}

		return self::connect()
					->insert($table, $no_duplication);
	}

	/**
	 * Make Repleace Query
	 *
	 * @param string $table - table name or JOINed tables, which has the same column names
	 *
	 * @return uDB\DBQueries
	 *
	 * @throws _InvalidArgumentException
	 *			if $table is empty or not string
	 **/
	public static function replace($table)
	{
		if (!is_string($table) || empty($table)) {
			throw new _InvalidArgumentException('$table must be not empty string');
		}

		return self::connect()
					->replace($table);
	}


	/**
	 * Make Delete Query
	 *
	 * @param string $table - table name or JOINed tables
	 *
	 * @return uDB\DBQueries
	 *
	 * @throws _InvalidArgumentException
	 *			if $table is empty or not string
	 **/
	public static function delete($table)
	{
		if (!is_string($table) || empty($table)) {
			throw new _InvalidArgumentException('$table must be not empty string');
		}

		return self::connect()
					->delete($table);
	}

	/**
	 * Get last insert id from DB
	 *
	 * @param string $connection_name - connection name
	 *
	 * @return int
	 *
	 **/
	public static function last_insert_id($connection_name = FALSE)
	{
		return self::connect($connection_name)
					->last_insert_id();
	}

	/**
	 * Crear all
	 *
	 * @param string|false $connection_name - name of connection, which needs to destroy
	 * 			if set FALSE - will destroy all connections
	 * 			if set NULL - will destroy current connections
	 *
	 * @return void
	 **/
	public static function clean($connection_name = FALSE)
	{
		if (is_null($connection_name)) {
			$connection_name = self::$current_connection_name;
		}

		if (!$connection_name || $connection_name == self::$current_connection_name) {
			self::$current_connection_name = FALSE;
		}

		uDB\DBQueries::clean($connection_name);
	}
}
