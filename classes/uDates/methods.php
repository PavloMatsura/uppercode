<?php
/**
 * Core class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 **/

namespace uDates;

/**
 * Class methods
 * Addition class, which works with dates
 *
 * @package uDates
 */
class methods {
	private $time_zone = NULL;
	private $date_format;

	/**
	 * @var \DateTime
	 */
	private $date_object;

	/**
	 * @param string $date_format
	 *
	 * @return \uDates\methods
	 */
	static public function init($date_format)
	{
		$object = new self();
		$object->dateFormat($date_format);

		return $object;
	}

	/**
	 * Set date format
	 *
	 * @param string $date_format
	 *
	 * @return \uDates\methods
	 */
	public function dateFormat($date_format)
	{
		if (!is_string($date_format)) {
			throw new \_InvalidArgumentException('Invalid $date_format');
		}

		$this->date_format = $date_format;

		return $this;
	}

	/**
	 * Set time zone
	 *
	 * @param string $time_zone
	 *
	 * @return \uDates\methods
	 */
	public function timeZone($time_zone)
	{
		$this->time_zone = \uDates::timeZone($time_zone);

		return $this;
	}

	/**
	 * Returns date string, according to time zone and date format
	 *
	 * @param false|string|int $timestamp - default value: currect TIMESTAMP
	 * @package false|string $date_format - if not set, will be used property $this->date_format
	 *
	 * @return string
	 */
	public function getDate($timestamp = FALSE, $date_format = FALSE)
	{
		if (!$timestamp) {
			$timestamp = time();
		} elseif (is_string($timestamp) && (int)$timestamp != $timestamp) {
			$timestamp = strtotime($timestamp);
		}

		if (!$date_format) {
			$date_format = $this->date_format;
		}

		$this->date_object = new \DateTime("@{$timestamp}", $this->time_zone);


		return $this->date_object->format($date_format);
	}

	/**
	 * @return \DateTime
	 */
	public function getObject()
	{
		return $this->date_object;
	}
}
