<?php
/**
 * Core class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

class uTPL
{
	private static $basic_tlp;

	/**
	 * @var \Twig_Environment[]
	 */
	private static $twig_instance;

	/**
	 * Set main template file name
	 * @param  string $tpl_file_name main template file name of current PAGE_TYPE
	 *
	 * @return void
	 */
	public static function setBasicTPL($tpl_file_name)
	{
		if (substr($tpl_file_name, -4) != '.php') {
			$tpl_file_name .= '.php';
		}

		self::$basic_tlp = $tpl_file_name;
	}

	/**
	 * Connect main or additional template file
	 * @param  boolean/string $tpl_file_name template file name
	 *
	 * @return boolean
	 */
	public static function connectBasicTPL()
	{
		$file_route = FALSE;

		if (!empty(self::$basic_tlp) && is_string(self::$basic_tlp) && file_exists(self::$basic_tlp)) {
			$file_route = self::$basic_tlp;
		} elseif (file_exists(THEME_ROUTE . mb_strtolower(PAGE_TYPE) . '.php')) {
			$file_route = THEME_ROUTE . mb_strtolower(PAGE_TYPE) . '.php';
		} elseif (file_exists(THEME_ROUTE . 'index.php')) {
			$file_route = THEME_ROUTE . 'index.php';
		}

		if ($file_route) {
			require_once $file_route;

			\uCode::run_action('page_has_been_loaded');

			exit();
		}

		throw new InvalidArgumentException('Basic template file of PAGE_TYPE "' . PAGE_TYPE . '" does not exists');
	}

	/**
	 * Connect main or additional template file
	 * @param  boolean/string $tpl_file_name template file name
	 *
	 * @return boolean
	 */
	public static function connect($tpl_file_name)
	{
		if (substr($tpl_file_name, -4) != '.php') {
			$tpl_file_name .= '.php';
		}

		if (!file_exists(THEME_ROUTE . $tpl_file_name)) {
			throw new InvalidArgumentException('Template file does not exists - "' . THEME_ROUTE . $tpl_file_name . '"');
		}

		require_once THEME_ROUTE . $tpl_file_name;

		return TRUE;
	}

	/**
	 * Include view file
	 * @param string $tpl_file_name - template file name
	 * @param string $module_name - module name, folder, where is template
	 * @param array $data - array of variables, what use in template
	 * @example
	 *    echo uTPL::get('posts_list', 'blog', array('title'=>'My new news') );
	 * 		//will return a content of file THEME_ROUTE . 'modules/blog_posts_list.php' , if it exists
	 *		//else APP_ROUTE . '_modules/blog/views/posts_list.php', if it exists
	 *		//esle return FALSE
	 *		//in this files will be available $vars_array. In this case $vars_array['title'] will be equal 'My new news'
	 *
	 * @return string
	 *
	 **/
	public static function get($tpl_file_name, $module_name, $data = [], $application = FALSE)
	{
		//Template in .php file
		if (substr($tpl_file_name, -4) === '.php') {
			$template_view = THEME_ROUTE . 'modules/' . $module_name. '_' . $tpl_file_name;
			$default_view = APP_ROUTE . '_modules/' . $module_name. '/_views/' . $tpl_file_name;

			if ($application) {//ToDo: Legasy confition, has to be removed
				$template_view = THEME_ROUTE . $module_name. '/' . $tpl_file_name;
				$default_view = APP_ROUTE . $module_name. '/_views/' . $tpl_file_name;
			}

			$file_route = $template_view;

			if (!is_file( $file_route )) {
				$file_route = $default_view;

				if (!is_file( $file_route )) {
					throw new InvalidArgumentException('Template "' . $file_route . '" was not found for module "' . $module_name . '" ');
				}
			}

			extract($data, EXTR_PREFIX_INVALID, 'index');

			$data_log = '';

			foreach ($data as $key => $value) {
				$data_log .= '$' . $key . ";\n";
			}

			ob_start();

			require $file_route;

			$tpl_file_name = ob_get_contents();

			ob_end_clean();

			return $tpl_file_name;
		}


		//Template in .twig file
		if (substr($tpl_file_name, -5) !== '.twig') {
			$tpl_file_name .= '.twig';
		}

		$file_route = APP_ROUTE . "_modules/{$module_name}/_views/";

		if (!is_file($file_route . $tpl_file_name)) {
			$file_route = THEME_ROUTE . 'modules/';
			$tpl_file_name = $module_name. '_' . $tpl_file_name;

			if (!is_file($file_route . $tpl_file_name)) {
				throw new InvalidArgumentException('Template "' . $file_route . '" was not found for module "' . $module_name . '" ');
			}
		}

		if (!(self::$twig_instance[$file_route] instanceof \Twig_Environment)) {
			$loader = new \Twig_Loader_Filesystem($file_route);

			self::$twig_instance[$file_route] = new \Twig_Environment($loader, [
				'cache' => (DEBUG_MODE !== true ? APP_ROUTE . '_cache/' : false),
			]);
			self::$twig_instance[$file_route]->addExtension(new Twig\AppExtension());
		}

		$template = self::$twig_instance[$file_route]->load($tpl_file_name);

		return $template
			->render($data);
	}

	/**
	 * Check existing of function and Run it, if TRUE
	 * @param  string $str 		string with name of function and paraneters of this function
	 * @example
	 * 		$str = '_getRandStr 7';
	 * 		$this->runShortCode($str);
	 * 		//will run function _getRandStr(7);
	 *
	 * @return string       	result of function work or shortcode
	 *
	 */
	protected static function runShortCode($str)
	{
		$var   = trim($str);
		$array = explode(' ', $var);

		if (function_exists($array[0])) {

			$var = trim(str_replace($array[0], '', $var));

			return $array[0]($var);

		} else {
			return '[:' . $str . ':]';
		}
	}

	/**
	 * Run function with the same name like name of shortcodes and replace it by result of function work
	 * @param  string $var string with sortcodes, like 'My name is [:UserName:]'
	 *
	 * @return string
	 */
	public static function shortCode($string)
	{
		preg_match_all("/\[\:(.*)\:\]/Uis", $string, $array);

		foreach ($array[1] as $key => $value) {
			$string = str_replace($array[0][$key], self::runShortCode($value), $string);
		}

		return $string;
	}

	/**
	 * Make error message
	 * @param string|array|object $message
	 * @param string $type - type of error
	 * @param bool $exit -
	 **/
	static public function message($message, $type = 'json', $exit = FALSE)
	{
		switch ($type) {
			case 'json':
				if (!headers_sent()) {
					header('Content-Type: application/json');
				}

				echo json_encode($message, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
			break;
			default:
				$error = debug_backtrace();
				unset($error[0]);

				_dump(['message' => $message,$error],'ERROR', $exit, 0, -1);
			break;
		}

		if ($exit) {
			exit();
		}
	}
}