<?php
/**
 * Constants defining
 *
 * Full list of basic constants
 * - HTTP_PROTOCOL
 * - BASIC_ROUTE - absolute server route
 * - APP_ROUTE - absolute server route
 * - THEME_ROUTE - absolute server route
 * - MEDIA_URL - `http` URL
 * - HOMEPAGE - `http` URL
 * - PAGE_URL - `http` URL
 * - CANONICAL_PAGE_URL - `http` URL
 */

//Define protocol
if(isset($_SERVER['HTTPS'])) {
	define('HTTP_PROTOCOL', 'https://');
} elseif (!defined('PAGE_TYPE') || PAGE_TYPE !== 'cron') {
	define('HTTP_PROTOCOL', 'http://');
} else {
	define('HTTP_PROTOCOL', null);
}

#region Constants validation

//Route to basic folder
if (!defined('BASIC_ROUTE')) {
	throw new InvalidArgumentException('Constant `BASIC_ROUTE` is not defined');
}

//Absolute server path to the application folder
if (!defined('APP_ROUTE')) {
	throw new InvalidArgumentException('Constant `APP_ROUTE` is not defined');
}

//Get basic config
$basic_config = require_once APP_ROUTE . 'basic_config.php';

if (defined('THEME_ROUTE')) {
	throw new InvalidArgumentException('Constant `THEME_ROUTE` cannot be defined outside of the framework');
}

if (defined('MEDIA_URL')) {
	throw new InvalidArgumentException('Constant `MEDIA_URL` cannot be defined outside of the framework');
}

if (defined('HOMEPAGE')) {
	throw new InvalidArgumentException('Constant `HOMEPAGE` cannot be defined outside of the framework');
}

if (defined('PAGE_URL')) {
	throw new InvalidArgumentException('Constant `PAGE_URL` cannot be defined outside of the framework');
}

if (defined('CANONICAL_PAGE_URL')) {
	throw new InvalidArgumentException('Constant `CANONICAL_PAGE_URL` cannot be defined outside of the framework');
}

if (defined('PAGE_URI')) {
	throw new InvalidArgumentException('Constant `PAGE_URI` cannot be defined outside of the framework');
}

#endregion

#region Define TPL constants

//Name of websites basic folder
if (!isset($basic_config['BASIC_FOLDER'])) {
	$basic_config['BASIC_FOLDER'] = '';
}

//Name of the custom theme folder
if (!isset($basic_config['THEME_FOLDER'])) {
	$basic_config['THEME_FOLDER'] = 'theme/';
}

//Absolute server path to the custom templates folder
define('THEME_ROUTE', BASIC_ROUTE . $basic_config['THEME_FOLDER']);

#endregion

//Break constants defining if is cron loading
if (defined('PAGE_TYPE') && PAGE_TYPE === 'cron') {
	define('MEDIA_URL', null);
	define('HOMEPAGE', null);
	define('PAGE_URL', null);
	define('CANONICAL_PAGE_URL', null);

	unset($basic_config);

	return;
}

#region Define media constants

//URL of the basic site folder
if (!isset($basic_config['MEDIA_URL'])) {
	$basic_config['MEDIA_URL'] = HTTP_PROTOCOL . $_SERVER['SERVER_NAME'] . '/' . $basic_config['BASIC_FOLDER'];
}

define('MEDIA_URL', $basic_config['MEDIA_URL']);

#endregion

#region Define `http` constants

//URL of homepage
define('HOMEPAGE', HTTP_PROTOCOL . $_SERVER['SERVER_NAME'] . '/' . $basic_config['BASIC_FOLDER']);


//URL of current page
define('PAGE_URL', HTTP_PROTOCOL . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);


//URL of current page without $_GET variables
define('CANONICAL_PAGE_URL', HTTP_PROTOCOL . $_SERVER['SERVER_NAME'] . (!isset($_SERVER['REDIRECT_URL']) ? '/' : $_SERVER['REDIRECT_URL']));

#endregion

unset($basic_config);
