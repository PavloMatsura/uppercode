<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Core controller
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

/**
 * Controller catcher - here is makes decision about exception level
 */

try {
	// Errors catchers
	if (defined('DEBUG_MODE') && DEBUG_MODE) {
		set_error_handler('_errors_tracker');
	}

	//Define settings and PAGE_TYPE
	uCode::init();

	if (PAGE_TYPE == PData::ERROR_PAGE_TYPE) {
		PData::redirect('',PData::ERROR_PAGE_TYPE);
	}

	uCode::run_action('defined_core');

	/** Connection applications files **/
	foreach (PData::config('include_apps') as $file_route => $allowed_page_types) {
		if (empty($allowed_page_types) || in_array(PAGE_TYPE, $allowed_page_types)) {
			if (!file_exists($file_route)) {
				throw new InvalidArgumentException('Required file "' . $file_route . '" does not exists');
			}

			require_once $file_route;
		}
	}

	uCode::run_action('defined_apps');

	#region Init Modules

	if (PData::config('include_modules')) {
		if (!is_array(PData::config('include_modules'))) {
			throw new InvalidArgumentException('Config `include_modules` is not array');
		}

		foreach (PData::config('include_modules') as $module_name => $allowed_page_types) {
			if (empty($allowed_page_types) || in_array(PAGE_TYPE, $allowed_page_types)) {
				$module_name = str_replace(['-'], '_', mb_strtolower($module_name));

				$module_class_name = "\\mod\\{$module_name}";

				/** @var \uController */
				$module_class_name::builder($module_name);
			}
		}
	}

	#endregion

	#region `defined_modules` - actions

	\uController::runModuleActions(\uController::ACT_DEFINED_MODULES);

	uCode::run_action('defined_modules');
	#endregion

	if (PAGE_TYPE == uAPI::PAGE_TYPE) {
		#region `init_api` - actions

		uCode::run_action('init_api');

		uAPI::init();

		uCode::run_action('api_init_completed');

		#endregion

		if (!PData::getPD('api_response')) {
			uAPI::setResponse('Error 404 - API method does not exist', FALSE);
		}

		$response = PData::getPD('api_response');

		uTPL::message($response, 'json');
	} else {
		#region `init_page` - actions

		\uController::runModuleActions(\uController::ACT_INIT_PAGE);

		uCode::run_action('init_page');
		uCode::run_action('page_init_completed');

		#endregion

		if (PAGE_TYPE === PData::CRON_PAGE_TYPE) {
			return;
		}

		//Connect page template
		uTPL::connectBasicTPL();
	}
} catch (RuntimeException $error) {
	uBugtracker::init()->add_track($error);

	if (PAGE_TYPE == uAPI::PAGE_TYPE) {
		uAPI::setResponse($error->getMessage(), FALSE);

		$response = PData::getPD('api_response');

		uTPL::message($response, 'json');
	} else {
		/** Set Message **/
		PData::setPD('message', $error->getMessage());

		#Connect page template
		uTPL::connectBasicTPL();
	}
} catch (Exception $error) {
	uBugtracker::init()->add_track($error);

	if ($error->getPrevious() != '') {
		$error = $error->getPrevious();
	}

	$message = array(
		0 => "<b>Description:</b>\n" .
			"============\n",
		1 => '',
	);
	$i = 0;

	foreach (explode("\n", $error->__toString()) as $error_item) {
		if (empty($error_item)) {
			continue;
		}

		if ($error_item == 'Stack trace:') {
			$i = 1;
			$message[$i] .=
				'<b>Stack trace:</b>' . "\n" .
				'============' . "\n";
		} elseif ($i != 1) {

			$i = 2;
			$error_item = explode(" in " . BASIC_ROUTE, $error_item);

			$message[0] .= htmlentities($error_item[0]) . "\n";

			if (count($error_item) > 1) {
				$message[0] .= "\n\n" .
					"<b>Throwed in:</b>\n" .
					"===========\n" . BASIC_ROUTE . $error_item[1] . "\n";
			}
		} elseif ($i == 1) {
			$message[$i] .= htmlentities($error_item) . "\n";
		}
	}

	$message = implode("\n\n", $message);
	$log_message = strip_tags($message) . "\n\n" . "Full stack trace:\n" . "=================\n" . print_r($error->getTrace(), 1);

	if (defined('DEBUG_MODE') && DEBUG_MODE) {
		___dump($message, 'Error Data: ' . date('Y-m-d H:i:s'), 1, 0);
	}
}

