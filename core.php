<?php
/**
 * Core of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

#Print all errors
if (defined('DEBUG_MODE') && DEBUG_MODE) {
	ini_set("display_errors", 1);
	error_reporting(E_ALL);
	ini_set('default_charset', 'UTF-8');
}


# Absolute server route to the core folder
define('CORE_ROUTE', __DIR__ . '/');

# Framework version
define('UPPERCODE_VERSION', '2.0.8');


require_once CORE_ROUTE . '_constants.php';

if (DEBUG_MODE && isset($_GET['phpinfo'])) {
	phpinfo();
	exit;
}

#connection core functions
require_once CORE_ROUTE.'_fn.php';

_trackTime('page_load_time');

//Connect Autoloader
require_once CORE_ROUTE . 'autoloader.php';

//Connection core controller
require_once CORE_ROUTE . '_controller.php';
