### Updates log

#### v 2.0.8
- Implemented Twig library
- Minor fixes


#### v 2.0.7
- Added parameters for `PAGE_TYPE === 'cron'`. E.g. if calls `php _applications/cron.php --paran1=value1 --param2=value2` in cmd, the passed parameters will be available in `echo PData::_GET('paran1'); echo PData::_GET('paran2');`. Also it using in core routing, as a `$_GET` elements in "web" mode
- Fixed autoloader for work with external classes (not framework classes), e.g. Composer generated classes
- Minor fixes


#### v 2.0.6
- Added CRON mode (PAGE_TYPE = 'cron') - in this mode framework does not loaded theme
- Refactored session init
- Refactored _fn.php
- Renamed `PData::HOME_PAGE_TYPE` to `PData::HOMEPAGE_TYPE`
- refactored time tracking function `_countTime()` and renamed it to `_trackTime()`
- Added `_trackMemory()` function to track used memory
- Minor fixes


#### v 2.0.5
- Added `uBugtracker` class
- Refactored controller and Exception catching
- Added default controller modules actions (interface of module class)
- Moved autoloader to single file,
- Removed class `uExceptions`
- Minor fix


#### v 2.0.4
- Refactored default constants defining
- Modify framework init interface (default settings array and required constants list)


#### v 2.0.3
- Added to uController interface Module actions (controller(), defined_modules(), init_page())
- Deprecated custom Exceptions
- Minor fixes

Date: 05.04.2018
