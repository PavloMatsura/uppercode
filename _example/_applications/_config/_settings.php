<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Framework Settings
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

return array(
	'homepage_settings' => array(
		'page_type' => 'homepage',
		'alias' => 'home',
		'exception_get_variables' => array(
			'partner',
			'utm_source',
			'utm_source',
			'utm_medium',
			'utm_campaign',
			'lang',
		),
	),
	'allowed_page_types' => array(	// Визначення допустимих типів сторінок
		'admin',
		'api',
	),

	'db_settings' => require_once __DIR__ . '/db.php',

	/**
	 * The list of allowed classes for each PAGE_TYPE.
	 * If class isn't set in this array - it is allowed for all PAGE_TYPEs.
	 *
	 * @example
	 *      'classes_allowed_rules' => array(
	 *          '\mod\my_module\my_module' => array('homepage'), // the class `my_module` from module `my_module` is allowed only for homepage
	 *      ),
	 **/
	'classes_allowed_rules' => array(
		'\mod\module_name\class_file_name' => array('allow for this page_type'),
	),

	/**
	 * The list of allowed library classes for each PAGE_TYPE.
	 * If class isn't set in this array - it is allowed for all PAGE_TYPEs.
	 *
	 * @example
	 *      'libs_allowed_rules' => array(
	 *          'my_lib' => array('homepage'), // the library `my_lib` is allowed only for homepage
	 *      ),
	 **/
	'libs_allowed_rules' => array(
		'lib_class_name' => array('allow for this page_type'),
	),

	/**
	 * This list determines, which application needs to be included
	 *
	 * @example
	 *      'include_apps' => array(
	 *			BASIC_ROUTE . 'vendor/autoload.php' => array(), //allowed for all PAGE_TYPEs
	 *      ),
	 **/
	'include_apps' => array(	// Підключає додаткові файли з дерикторії аплікацій
		// BASIC_ROUTE . 'vendor/autoload.php' => array(),
	),

	#APP_ROUTE.'_modules/'
	/**
	 * Module name is a name of module controller class and it can consist of only the next symbols /a-zA-Z0-9_/
	 **/
	'include_modules' => array(
		'example_module' => array(),
	),

	/**
	 * Callback functions which will calling after config defined
	 * 
	 * @example 
	 * 		'init_callbacks' => [
	 * 			//Function calling format
	 * 			'functionName' => [
	 * 				'param1' => 1,
	 * 				'param2' => 2,
	 * 			],
	 * 
	 * 			//Method calling format
	 * 			[
	 * 				'class_name' => 'my_class',
	 * 				'method_name' => 'my_method',
	 * 				'param1' => 1,
	 * 				'param2' => 2,
	 * 				//...
	 * 			],
	 * 		],
	 */
	'init_callbacks' => [],
);