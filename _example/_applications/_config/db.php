<?php if (!defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * DataBase keys
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

return [
	'default' => [
		'host' => 'localhost',
		'user' => 'root',
		'pass' => '',
		'db_name' => 'db_uppercode',
	],
];