<?php

namespace mod;

class my_module extends \uController
{
	protected function init_module() {}

	protected function defined_modules() {
		\uTPL::setBasicTPL('error_page.php');

		$my_module_object = \mod\my_module::builder(); // get module object, it needs only for static methods, in object methods you should use `$this` variable

		$my_module_object->getModel('example_model.php'); //this method will connect file `APP_ROUTE . '_modules/my_module/_models/example_model.php'` (model from current module folder)
	}

	protected function init_page() {}
}
