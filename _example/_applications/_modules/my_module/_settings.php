<?php
/**
 * 'URL_rules' => [
 *      'page' => '{page_alias}/',
 *      'post' => '{category_alias}/{post_alias}.html',
 *      'category' => 'category/{alias}/',
 * ],
 * 'allowed_for_page_types' => [
 *      'homepage',
 * ],
 **/
return array(
	'URL_rules' => array(
		'category' => 'cat/{category_alias}/[post_alias]/',
		'page' => '{page_alias}/',
		'post' => '{category_alias}/{post_alias}.html',
	),

	'allowed_for_page_types' => array(
		PData::HOMEPAGE_TYPE,
	),
);