<?php

return [
	/**
	 * Name of basic website folder - should ending with '/' or be empty
	 * Use to put website in subfolder.
	 * This constant used in constant `HOMEPAGE`
	 *
	 * @example
	 *		if set 'test/'
	 *			- website URL will be http://examlpe.com/test/
	 */
	'BASIC_FOLDER' => null,

	/**
	 * Route to folder with media - should starting for 'http://' and ending on '/'. Set NULL to turn on default value.
	 * Use for include themes style from other folder or domain
	 *
	 * @example
	 *		the expression {MEDIA_URL}medias/ is the same as http://example.com/test/medias/
	 *
	 * @default - link to current basic website folder
	 */
	'MEDIA_URL' => null,

	/**
	 * Route to folder with theme - should ending on '/'. Set NULL to turn on default value.
	 *
	 * @example
	 *		if set 'theme/',
	 * 				- the expression {BASIC_MEDIA_URL}{THEME_FOLDER} is the same http://example.com/test/theme/
	 *
	 * @default 'theme/'
	 */
	'THEME_FOLDER' => null,

    /**
     * The name (with namespace) of custom bugtracker class
     * It has to follow the core class `uBugtracker`
     */
    'bugtracker_class' => 'my_bugtracker',
];
