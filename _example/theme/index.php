<?php

$to_page_type = (PData::isHomepage() ? 'post' : PData::HOMEPAGE_TYPE);

$url = HOMEPAGE;

if (PData::isHomepage()) {
	$url = PData::getURL('post', ['category_alias' => 'news','post_alias' => 'elvis-is-alive']);
}

echo '<a href="'.$url.'">' . $to_page_type . '</a>';

_trackTime('page_load_time');
echo '<br>' . _trackTime(true);