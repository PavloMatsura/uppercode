<?php
/**
 * Absolute route to basic folder - should ending with '/'.
 * If needs to move Core folder into other place, set this parameter in __DIR__ . '/'
 *
 * @default - it does not have a default value, because this constant is required
 */
define('BASIC_ROUTE', __DIR__ . '/');

/**
 * Route to Application folder - should ending with '/'.
 *
 * @example
 *		if set '_applications/', application files, like modules, will be located in folder {BASIC_ROUTE}_applications/
 *		It does not available to follow it through direct link.
 */
define('APP_ROUTE', BASIC_ROUTE . '_applications/');

/**
 * Set TRUE to turn on debug mode. It let to see all warning through browser's window
 */
if (!defined('DEBUG_MODE')) {
	define('DEBUG_MODE', TRUE);
}

/**
 * Set TRUE to turn on log saving in core folder
 */
if (!defined('SAVE_LOG')) {
	define('SAVE_LOG', TRUE);
}

#Require the framework
require_once BASIC_ROUTE . '../core.php';
