<?php
/**
 * Core functions of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

/**
 * @param $errno
 * @param $errstr
 * @param $errfile
 * @param $errline
 * @param $err_5
 * @throws Exception
 */
function _errors_tracker($errno, $errstr, $errfile, $errline, $err_5)
{
	throw new \Exception(implode("\n",array($errstr, $errfile.':'. $errline)));
}

function _cache( $step=FALSE, $minutes=1 )
{
	if(!$step) return FALSE;

	$minutes = $minutes*60;

	if(!defined('CACHE_TIME') && $step!='end' ){
		define('CACHE_TIME',$minutes);
	}elseif(defined('CACHE_TIME')){
		$minutes = @CACHE_TIME;
	}

	$time=floor( time()/$minutes ) * $minutes;
	$file_name = CORE_ROUTE.'logs/cache/'.urlencode(md5(PAGE_URL.$time)).'.cache';
	$old_time = (floor( time()/$minutes ) * $minutes) - $minutes;
	$old_file_name = CORE_ROUTE.'logs/cache/'.urlencode(md5(PAGE_URL.$old_time)).'.cache';

	if( $step == 'start' ){
		if( file_exists( $file_name ) ){
			readfile($file_name);
			exit();
		}

		ob_start();

	}elseif( $step == 'end' && defined('CACHE_TIME')){
		if (date('H')<=3) {
			_unDir(CORE_ROUTE.'logs/cache/',FALSE);
		} else {
			unlink($old_file_name);
		}

		$buffer = ob_get_contents();

		ob_end_flush();

		$fp = fopen($file_name, 'w');
		fwrite($fp, $buffer);
		fclose($fp);
	}
}

#Send mail
function _eMail($mail, $title, $text,$from='')
{
	if (empty($from)) {
		$from=_getDomain(HOMEPAGE).' <no-replay@'._getDomain(HOMEPAGE).'>';
	}

	$headers  = "Content-type: text/html; charset=utf-8 \r\n";
	$headers .= "From: $from\r\n";
	//	$headers .= "Bcc: no-reply1@m-mba.com.ua\r\n";
	$mails = explode(',',$mail);
	$errors = 0;

	if (count($mails) > 1) {
		foreach ( $mails as $row ) {
			if(!mail(trim($row), $title, $text, $headers) || !_checkEmail(trim($row))) $errors++;
		}
	} else {
		if(!mail(trim($mails[0]), $title, $text, $headers) || !_checkEmail(trim($mails[0]))) $errors++;
	}

	if ($errors > 0) {
		return false;
	} else {
		return true;
	}
}

#region Getter functions

/**
 * Return URL string
 * Is wrapper function of PData::getURL();
 *
 * @param string $PAGE_TYPE - page type of URL rule
 * @param array|mixed $URL_data - can has array or scalar value
 *
 * @return string
 */
function _getURL($PAGE_TYPE, $URL_data = false)
{
	return PData::getURL($PAGE_TYPE, $URL_data);
}

#повертає масив, де перший елемент, домен
function _getDomain($val, $array = FALSE)
{
	$url = parse_url($val);

	if($array) {
		return $url;
	} else {
		return @$url['host'];
	}
}

function _getIP()
{
	if(isset($_SERVER['HTTP_X_REAL_IP'])) {
		return $_SERVER['HTTP_X_REAL_IP'];
	}
	return $_SERVER['REMOTE_ADDR'];
}

function _getActive($param, $val = FALSE, $return = 'active')
{
	if($val && $param == $val){
		return $return;
	}
	return FALSE;
}

/**
 * Return random string
 * @param int $long - size of returned string
 * @param string|false $type - type of returned string. Can be 'str', 'int' or other
 *
 * @return string
 *
 * @throws InvalidArgumentException
 *          if $long is not integer or less then 1
 *
 */
function _getRandStr($long = 6, $type = FALSE)
{
	if (!is_int($long) || $long < 1) {
		throw new InvalidArgumentException('$long must be ineger and more then 0');
	}

	$array = array(
		'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
		'0','1','2','3','4','5','6','7','8','9',
	);

	switch($type){
		case'str':
			$array = array(
				'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
			);
			break;
		case'int':
			$array = array(
				'0','1','2','3','4','5','6','7','8','9',
			);
			break;
	}

	$string = '';
	$last_key_numb = count($array) - 1;

	for ($i = 0; $i < $long; $i++) {
		$string .= $array[rand(0, $last_key_numb)];
	}

	return $string;
}

#endregion

#region Tracking functions

/**
 * Returns time in seconds with milliseconds
 *
 * @return float
 */
function _mTime()
{
	list($usec, $sec) = explode(' ', microtime());

	return (float) $usec + (float) $sec;
}

/**
 * Counting time of a script execution speed
 *
 * @example
 *    _trackTime('Time of testing');
 *        //some code
 *    echo _trackTime('Time of testing');
 *    echo _trackTime(true);
 *
 * @param string|true $script_key
 * @param int $number_size
 * @param bool $show_unit - TRUE if needs to return unit of measurement (b)
 *
 * @return string
 *
 */
function _trackTime($script_key = 'script_load_time', $number_size = 6, $show_unit = true)
{
	if (!is_string($script_key) && $script_key !== true) {
		throw new InvalidArgumentException('Invalid $script_key');
	}

	$number_size = (int) $number_size;

	if ($number_size < 1) {
		$number_size = 6;
	}

	global $trackTime;

	if ($script_key === true) {
		$result = '';

		if ($trackTime) {
			foreach ($trackTime as $script_key => $time) {
				$result .= $script_key . ': ' . _trackTime($script_key, $number_size, $show_unit) . "\n";
			}
		}

		return $result;
	}

	if(!isset($trackTime[$script_key]['start'])){
		$trackTime[$script_key]['start'] = _mTime();
		$trackTime[$script_key]['time'] = 0;
	}else{
		$trackTime[$script_key]['time'] += (_mTime() - $trackTime[$script_key]['start']);

		return number_format($trackTime[$script_key]['time'], $number_size) . ($show_unit ? ' s' : '');
	}

	return number_format($trackTime[$script_key]['time'], $number_size) . ($show_unit ? ' s' : '');
}

/**
 * Tracks used memory
 *
 * @note !!! the function can be called only two times with the same value of $script_key or it will returns not relevant data
 *
 * @example
 * 		_trackMemory('my_script'); //start it track used memory
 * 		//some code
 * 		echo _trackMemory('my_script'); //print value of used memory
 *
 * 		//Print all collected tracks
 * 		echo _trackMemory(true);
 *
 * @param string|true $script_key - title of script part
 * @param bool $show_unit - TRUE if needs to return unit of measurement (b)
 *
 * @return string
 */
function _trackMemory($script_key = 'script_load_memory', $show_unit = true)
{
	if (!is_string($script_key) && $script_key !== true) {
		throw new InvalidArgumentException('Invalid $script_key');
	}

	global $trackMemory;

	if ($script_key === true) {
		$result = '';

		if ($trackMemory) {
			foreach ($trackMemory as $script_key) {
				$result .= $script_key . ': ' . _trackMemory($script_key, $show_unit) . "\n";
			}

			$result .= 'Peak: ' . memory_get_peak_usage() . ($show_unit ? ' b' : '');
		}

		return $result;
	}

	if (!isset($trackMemory[$script_key])) {
		$trackMemory[$script_key] = - memory_get_usage();
	} else {
		$trackMemory[$script_key] += memory_get_usage();
	}

	if ($trackMemory[$script_key] < 0) {
		return 0 . ($show_unit ? ' b' : '');
	}

	return $trackMemory[$script_key] . ($show_unit ? ' b' : '');
}

#endregion

#region Convertation functions

function _textToURL($title, $switch = TRUE)
{
	$title = trim($title);

	if ($switch) {//Тренслітерує строку
		$tranclit = array(
			"А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Ґ"=>"g","Д"=>"d","Е"=>"e","Є"=>"ye","Ё"=>"e","Ж"=>"zh","З"=>"z","И"=>"i","І"=>"i","Ї"=>"yi","Й"=>"j","К"=>"k", "Л"=>"l","М"=>"m","Н"=>"n","О"=>"o","П"=>"p", "Р"=>"r","С"=>"s","Т"=>"t","У"=>"u","Ф"=>"f", "Х"=>"h","Ц"=>"ts","Ч"=>"ch","Ш"=>"sh","Щ"=>"sch", "Ы"=>"y","Э"=>"e","Ю"=>"yu","Я"=>"ya",
			"Ь"=>"","Ъ"=>"",
			"а"=>"a","б"=>"b","в"=>"v","г"=>"g","ґ"=>"g","д"=>"d","е"=>"e","є"=>"ye","ё"=>"e","ж"=>"zh","з"=>"z","и"=>"i","і"=>"i","ї"=>"yi","й"=>"j","к"=>"k", "л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p", "р"=>"r","с"=>"s","т"=>"t","у"=>"u","ф"=>"f", "х"=>"h","ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch", "ы"=>"y","э"=>"e","ю"=>"yu","я"=>"ya",
			"ь"=>"", "Ъ"=>"",

			") "=>"", " ("=>"", " - "=>"-", " "=>"-", "$"=>"", "@"=>"", "!"=>".", "?"=>".", "&"=>"",
			"="=>"", "|"=>"", "/"=>"", "\\"=>"", "#"=>"", "\""=>"", "'"=>"", ";"=>"", ":"=>"", ", "=>"-", ","=>"",
			"."=>".", "+"=>"_", "("=>"", ")"=>"", "*"=>"", "^"=>"", "№"=>"", ">"=>"", "<"=>"", "%"=>"", "`"=>"",
			']'=>'', '['=>'', '{'=>'', '}'=>'',"»"=>'',"«"=>''
		);
		return strtr($title, $tranclit);

	} else {//мало б перекладає строку, але не робить цього (
		$t = @json_decode(@file_get_contents('http://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20130926T104523Z.30991179ca1db141.c9a1ee38c42a525b5365a590fa78bc877ad4a489&text='.urlencode('Мама').'&lang=en'),TRUE);

		if ($t) {
			$title = $t['text'];
		}
		return _textToURL($title,FALSE);
	}
}

/**
 * Prepare string or array for URL
 **/
function _prepareStr($string, $encode = TRUE, $json = TRUE)
{
	if ($json) {
		if ($encode) {
			return urlencode( base64_encode( json_encode($string) ) );
		} else {
			return json_decode( base64_decode( urldecode($string) ), TRUE );
		}
	} else {
		if ($encode) {
			return urlencode( base64_encode($string) );
		} else {
			return base64_decode( urldecode($string) );
		}
	}
}

/**
 * Convert result of print_r back to array
 * @link http://php.net/manual/en/function.print-r.php#93529
 *
 * @param string $string
 *
 * @return array|string
 */
function _print_r_reverse($string)
{
	$lines = explode("\n", trim($string));

	if (trim($lines[0]) != 'Array') {
		// bottomed out to something that isn't an array
		return $string;

	} else {
		// this is an array, lets parse it
		if (preg_match("/(\s{5,})\(/", $lines[1], $match)) {

			// this is a tested array/recursive call to this function
			// take a set of spaces off the beginning
			$spaces = $match[1];
			$spaces_length = strlen($spaces);
			$lines_total = count($lines);

			for ($i = 0; $i < $lines_total; $i++) {
				if (substr($lines[$i], 0, $spaces_length) == $spaces) {
					$lines[$i] = substr($lines[$i], $spaces_length);
				}
			}
		}

		array_shift($lines); // Array
		array_shift($lines); // (
		array_pop($lines); // )

		$string = implode("\n", $lines);

		// make sure we only match stuff with 4 preceding spaces (stuff for this array and not a nested one)
		preg_match_all("/^\s{4}\[(.+?)\] \=\> /m", $string, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);

		$pos = array();
		$previous_key = '';
		$string_length = strlen($string);

		// store the following in $pos:
		// array with key = key of the parsed array's item
		// value = array(start position in $string, $end position in $string)
		foreach ($matches as $match) {
			$key = $match[1][0];
			$start = $match[0][1] + strlen($match[0][0]);
			$pos[$key] = array($start, $string_length);

			if ($previous_key != '') {
				$pos[$previous_key][1] = $match[0][1] - 1;
			}

			$previous_key = $key;
		}

		$return = array();

		foreach ($pos as $key => $where) {
			// recursively see if the parsed out value is an array too
			$return[$key] = _print_r_reverse(substr($string, $where[0], $where[1] - $where[0]));
		}

		return $return;
	}
}

/**
 * Converts string like '2M' to bytes
 *
 * @param string $string
 *
 * @return string|false
 **/
function _return_bytes($string)
{
	$result = trim($string);
	$last = strtolower($result[strlen($result)-1]);

	switch($last) {
		case 'g': $result *= 1024;
		case 'm': $result *= 1024;
		case 'k': $result *= 1024;
			break;

		default:
			return false;
	}

	return $result;
}

#endregion

/**
 * Run function with the same name like name of shortcodes and replace it by result of function work
 * @param  string $var string with sortcodes, like 'My name is [:UserName:]'
 * @return string
 */
function _shortCode($string)
{
	/**
	 * Check existing of function and Run it, if TRUE
	 * @param  string $str 		string with name of function and paraneters of this function
	 * @example
	 * 		$str = '_getRandStr 7';
	 * 		$runShortCode($str);
	 * 		//will run function _getRandStr(7);
	 *
	 * @return string       	result of function work or shortcode
	 *
	 */
	$runShortCode = function ($str) {
		$var = trim($str);
		$array = explode(' ', $var);

		if (function_exists($array[0])) {
			$var = trim(str_replace($array[0], '', $var));

			return $array[0]($var);
		} else {
			return '[:' . $str . ':]';
		}
	};

	preg_match_all("/\[\:(.*)\:\]/Uis", $string, $array);

	foreach ($array[1] as $key => $value) {
		$string = str_replace($array[0][$key], $runShortCode($value), $string);
	}

	return $string;
}

/**
 * @param string $str
 * @param array $array = [
 *			        'replease_key' => 'replease_value',
 *			    ];
 *
 * @return string
 **/
function _sharpCode($str, $array)
{
	if (is_array($array)) {
		foreach($array AS $key=>$val){
			$str = str_replace($key,$val,$str);
		}
	}
	return $str;
}



#region Dump functions

/**
 * Print string in draggable popup window
 *
 * @see ___dump() - Use this function if you are offline and/or _dump() doesn't work correct
 *
 * @param  string | array  $str       - string, what will print
 * @param  string  $var_title - title of window
 * @param  boolean $exit      - does script need to exit?
 * @param  boolean $details   - print string throught pring_r (default) or var_dump
 */
function _dump($str, $var_title = 1, $exit = FALSE, $var_dump = FALSE, $title_style = '#d3d3d3')
{
	if (!$var_dump){
		$buff = print_r($str, TRUE);
	}else{
		ob_start();
		var_dump($str);
		$buff = ob_get_clean();
		ob_end_clean();
	}

	$debug_backtrace = debug_backtrace();
	$file_route = '<div style="font-size:12px;border-top: 1px solid #898888;margin: 10px -10px -10px;padding: 10px 10px 11px;background: #e7e7e7;width: 100%;">Route: <input style="width: 190px;" value="' . $debug_backtrace[0]['file'].':'.$debug_backtrace[0]['line'] . '"/><p>Loaded in: ' . _trackTime('page_load_time') . ' s</p></div>';

	if($var_title == 1) {
		$var_title = '<b>#' . $debug_backtrace[0]['line'] . '</b>';
	}


	switch ($title_style) {
		case -1:
			$title_style = '#FFCBCB';
		break;

		default:
			$title_style = '#d3d3d3';
		break;
	}

	$dumps_html = '
	<style type="text/css">
			.dump-cl {
					min-width: 250px !important;
					height: auto !important;
					width: auto !important;
					max-width: 90% !important;
					font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			}
			.dump-cl pre{
					padding: 0;
					margin: 0;
					font-size: 13px;
					line-height: normal;
					word-break: normal;
					word-wrap: normal;
					background: none;
					border: 0;
					border-radius: 0;
					font-weight: normal;
					white-space: pre-wrap;
			}
	</style>

	<div class="dump-cl" style="top: 10px;left: 10px; position: absolute;z-index: 999999;border-radius: 10px;overflow: hidden;background-color: white;box-shadow: 1px 1px 5px #000;">
			<div class="dump-h" style="background-color: ' . $title_style .';padding: 10px 10px;border-bottom: 1px solid #000;vertical-align: top;cursor: move;"><span class="dump-hide" style="cursor:pointer;font-size: 35px;position: absolute;top: -3px;right: 0;padding: 0px 12px 3px;display: block;">-</span>
				<span style="margin-right: 15px;">' . $var_title . '</span></div>
			<div class="dump-cont" style="padding: 10px;">
					<pre style="margin: 0;">'
		. $buff .
		'</pre>
					'.$file_route.'
			</div>
	</div>';

	if (!defined('_DUMPS_')){

		define('_DUMPS_','1');
		$dumps_html .= '<script type="text/javascript">
			if(!window.jQuery){
					document.writeln(
							\'<sc\'+\'ript sr\'+\'c="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"><\'+\'/\'+\'sc\'+\'ript\'+\'>\'
					);
					document.writeln(
							\'<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css">\'+
							\'<sc\'+\'ript sr\'+\'c="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"><\'+\'/\'+\'sc\'+\'ript\'+\'>\'
					);
			}
			/*if (typeof jQuery == "function" && (\'ui\' in jQuery) && jQuery.ui && (\'version\' in jQuery.ui))
			{

			}*/
	</script>
	<script type="text/javascript">
			jQuery(document).ready(function(){
					jQuery( ".dump-cl" ).draggable({ handle: ".dump-h" });
					jQuery( ".dump-cl" ).click(function(){
						$(".dump-cl").css("z-index", "999999");
						$(this).css("z-index", "9999999");
					});
					jQuery(".dump-h .dump-hide").click(function(){
							jQuery(this).parents(".dump-cl").find(".dump-cont").toggle("slow");
					});
			});

			</script>';
	}

	echo $dumps_html;

	if ($exit) {
		exit();
	}
}

/**
 * Makes dump which is adapt for review in browser consol log  - consol.log(dump_data);
 *
 * @return string|false|void
 **/
function __dump($str, $var_title = 1, $exit = FALSE, $var_dump = FALSE, $echo = TRUE)
{
	if (!$var_dump) {
		$buff = print_r($str, true);
	} else {
		ob_start();
		var_dump($str);
		$buff = ob_get_clean();
	}

	$debug_backtrace = debug_backtrace();
	$file_route = 'Route: ' . $debug_backtrace[0]['file'] . ':' . $debug_backtrace[0]['line'];

	if ($var_title == 1) {
		$var_title = 'line #' . $debug_backtrace[0]['line'] . '';
	}

	$dumps_html =
		"\nDeBuging Dump\n" .
		"=============\n" .
		"Title: " . $var_title . "\n" .
		"===================================\n\n" .
		htmlentities($buff) . "\n\n" .
		"===================================\n" .
		$file_route . "\n" .
		"Loaded in: " . _trackTime('page_load_time') . " s\n" .
		"\n\n";

	if ($echo) {
		echo $dumps_html;
	} else {
		return $dumps_html;
	}

	if ($exit) {
		exit();
	}
}

/**
 * Makes dump which is adapt for review on browser web page and in page source;
 * Use this function if you are offline and/or _dump() doesn't work correct
 *
 * @return void
 **/
function ___dump($str, $var_title = 1, $exit = FALSE, $var_dump = FALSE)
{
	if (!$var_dump) {
		$buff = print_r($str, true);
	} else {
		ob_start();
		var_dump($str);
		$buff = ob_get_clean();
	}

	$debug_backtrace = debug_backtrace();
	$file_route = 'Route: ' . $debug_backtrace[0]['file'] . ':' . $debug_backtrace[0]['line'];

	if ($var_title == 1) {
		$var_title = 'line #' . $debug_backtrace[0]['line'] . '';
	}

	$dumps_html =
		"<pre style=\"margin: 0;\">\n" .
		"DeBuging Dump\n" .
		"=============\n" .
		"Title: " . $var_title . "\n" .
		"===================================\n\n" .
		$buff . "\n\n" .
		"===================================\n" .
		$file_route . "\n" .
		"Loaded in: " . _trackTime('page_load_time') . " s\n" .
		"\n\n</pre>";

	echo $dumps_html;

	if ($exit) {
		exit();
	}
}

#endregion

#region Is condition functions

/**
 * Wrapper of method uCode::isHomepage()
 *
 * @return bool
 **/
function _isHomepage()
{
	return PData::isHomepage();
}

function _isPageType($page_type) {
	return \PData::isPageType($page_type);
}

#вертае False при хибному значенні пошти.
function _checkEmail($email,$switch=FALSE)
{
	$emails = explode(',',$email);
	$errors = 0;

	if (count($emails) > 1)	{
		foreach ( $emails as $mail ) {
			if (!filter_var(trim($mail), FILTER_VALIDATE_EMAIL)) {
				$errors++;
			}
		}
	} else {
		if (!filter_var(trim($emails[0]), FILTER_VALIDATE_EMAIL)) {
			$errors++;
		}
	}
	if ($errors > 0) {
		return false;
	} else {
		return true;
	}
}

function _checkURL($url, $alt = FALSE)
{
	$check = @get_headers($url);

	if($check[0]=='HTTP/1.1 200 OK'){
		return $url;
	}
	return$alt;
}

#endregion

#region Deprecated functions

/**
 * @deprecated
 * Set exceptions of $_GET variables for define homepage
 * @param  string  $page_type 		- Page Type of homepage
 * @param  string  $val      		- value of variable $_GET[PAGE_TYPE]
 * @param  boolean/array $array 	- array with $_GET exceptions
 *
 * @return boolean            		- TRUE - homepage, FALSE - not hompage
 */
function _define_homepage_vars($page_type, $val, $array=FALSE)
{
	$get = @$_GET;
	$g = array();

	if(is_array($array)){
		foreach($array AS $v){//отримуємо перелік доступних змінних і записуємо в тимчасовий масив
			if(isset($_GET[$v])){
				$g[$v] = $_GET[$v];
				unset($_GET[$v]);
			}
		}
	}

	if(_isHomepage() && empty($_GET[$page_type]) && !defined('PAGE_TYPE')){ //перевіряємо, чи головна сторынка
		define('PAGE_TYPE',$page_type);
		$_GET[$page_type]=$val;
	}else{
		$_GET = array_merge($_GET, $get); //повертаэмо назад значення масиву $_GET
		return FALSE;
	}

	$_GET = array_merge($_GET, $get); //повертаэмо назад значення масиву $_GET

	return TRUE;
}

/**
 * @deprecated
 * @see uCode::add_action($key, $function, $priority);
 */
function _add_action($key, $function, $priority = 10)
{
	return uCode::add_action($key, $function, $priority);
}

/**
 * @deprecated
 * @see uCode::add_filter($key, $function, $priority);
 */
function _add_filter($key, $function, $priority = 10)
{
	return uCode::add_filter($key, $function, $priority);
}

# Initializing all functions, what was added to current action
/**
 * @deprecated
 * @see uCode::run_action($key, $accepted_vars);
 **/
function _run_action($key, $accepted_vars = '')
{
	return uCode::run_action($key, $accepted_vars);
}

/**
 * @deprecated
 * @see uCode::run_action($key, $accepted_vars);
 **/
function _run_multi_action($key, $accepted_vars = '')
{
	return uCode::run_multi_action($key, $accepted_vars);
}

# запускає хук в середині функції
/**
 * @deprecated
 * @see uCode::run_filter($key, $accepted_vars);
 **/
function _run_filter($key, $result = '', $accepted_vars = '')
{
	return uCode::run_filter($key, $result, $accepted_vars);
}

/**
 * @deprecated
 * @see uCode::run_filter($key, $accepted_vars);
 **/
function _run_multi_filter($key, $result = '', $accepted_vars = '')
{
	return uCode::run_multi_filter($key, $result, $accepted_vars);
}

#endregion

/**
 * Makes correct merging between 2 arrays, which has the sane first element
 *
 * @param array $array1 - basic array
 * [
 *		'elenemt1' => [
 * 			'123' => [
 * 				'789',
 * 			],
 * 			'456',
 * 		]
 * 		'element2' => 2,
 * 		'element3' => 3,
 * ]
 *
 * @param array $array2 - new array values
 * [
 * 		'elenemt1' => [
 * 			'234' => 'ertw',
 * 		],
 * ]
 *
 * @return array
 * [
 *		'elenemt1' => [
 * 			'123' => [
 * 				'789',
 * 			],
 * 			'456',
 * 			'234' => 'ertw', // this element was added
 * 		]
 * 		'element2' => 2,
 * 		'element3' => 3,
 * ]
 *
 **/
function _arrays_adding($array1, $array2)
{
	if (!is_array($array1) || !is_array($array2)) {
		throw new InvalidArgumentException('$array1 and $array2 must be array');
	}

	$i = 0;

	foreach ($array2 as $key => $value) {
		if (isset($array1[$key])) {
			if (is_array($value) && !empty($value)) {
				$array1[$key] = _arrays_adding($array1[$key], $value);
			} else {
				$array1[$key] = $value;
			}
		} elseif ($i == 0) {
			$array1 += $array2;

			break;
		}

		$i++;
	}

	return $array1;
}

/****************** PHP old *********************/
if (!function_exists("array_column")) {
	/**
	 * @return array
	 **/
	function array_column($array, $column_name) {
		$result = [];

		foreach ($array as $value) {
			if (isset($value[$column_name])) {
				$result[] = $value[$column_name];
			}
		}

		return $result;
	}
}
